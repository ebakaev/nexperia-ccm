(function () {

    var preloaderInstance;
    var preloaderStart = function () {
        preloaderInstance = Alfresco.util.PopupManager.displayMessage({
            text: "Checking Ordering Information topic...",
            modal: true,
            displayTime: 0,
            spanClass: "wait",
            effect: null
        });
    };

    var preloaderStop = function() {
        try{
            if (preloaderInstance) {
                preloaderInstance.destroy();
            }
        }catch(e) {}
    };

    var callDitaMapReleaseAction = function (record) {
        var toPdfUrl;
        if (Alfresco.constants.PROXY_URI.indexOf("qa.") !== -1){
            toPdfUrl = "https://qa.eptos.pim.ssg.nexperia.com/ssg/preview.html?type=ccmshare&custom="+record.webdavUrl.replace("/webdav/","")+"&nodeRef="+record.nodeRef+"&purgecache=yes&user=ssg&password=%40Nex17%23peria";
        } else {
            toPdfUrl = "https://www.eptos.pim.ssg.nexperia.com/ssg/preview.html?type=ccmshare&custom="+record.webdavUrl.replace("/webdav/","")+"&nodeRef="+record.nodeRef+"&purgecache=yes&user=ssg&password=LRS%26Rq27PRnr4p%21L";
        }
        window.open(toPdfUrl, "_blank");
    };

    YAHOO.Bubbling.fire("registerAction", {
        actionName: "ditaToPreviewAndRelease",
        fn: function ditamap2ssgpdf(record) {
            preloaderStart();
            Alfresco.util.Ajax.jsonGet({
                url: Alfresco.constants.PROXY_URI + "nexperia/checkTopic?nodeRef=" + record.nodeRef,
                successCallback: {
                    fn: function onCheckTopic_Success(response) {
                        preloaderStop();
                        YAHOO.Bubbling.fire("metadataRefresh");
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["message"];
                            if (message === 'valid') {
                                callDitaMapReleaseAction(record);
                            } else {
                                Alfresco.util.PopupManager.displayPrompt(
                                    {
                                        title: "Ordering information topic validation failed...",
                                        text: "Ordering information topic contains wrong links!\n Do you want to continue?",
                                        buttons: [
                                            {
                                                text: "Continue",
                                                handler: function close_screen()
                                                {
                                                    callDitaMapReleaseAction(record);
                                                    this.destroy();
                                                }
                                            },
                                            {
                                                text: "Cancel",
                                                handler: function close_cancel()
                                                {
                                                    this.destroy();
                                                },
                                                isDefault: true
                                            }]
                                    });
                            }
                        } else {
                            callDitaMapReleaseAction(record);
                        }
                    },
                    scope: this
                },
                failureCallback: {
                    fn: function (response) {
                        preloaderStop();
                        callDitaMapReleaseAction(record);
                    },
                    scope: this
                }
            });
        }
    });
})();
