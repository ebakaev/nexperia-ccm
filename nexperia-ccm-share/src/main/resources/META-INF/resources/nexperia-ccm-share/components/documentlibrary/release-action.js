(function()
{

    var preloaderInstance;
    var preloaderStart = function (){
        preloaderInstance = Alfresco.util.PopupManager.displayMessage({
            text: "Please wait...",
            modal: true,
            displayTime: 0,
            spanClass: "wait",
            effect: null
        });
    };

    var preloaderStop = function(){
        try{
            if (preloaderInstance) {
                preloaderInstance.destroy();
            }
        }catch(e) {}
    };

    var displayFailure = function(response) {
        Alfresco.util.PopupManager.displayPrompt({
            title: "Failure",
            text: response
        });
    };

    var displayMessage = function (message, displayingTextProp) {
        if (message == "Success") {
            Alfresco.util.PopupManager.displayMessage({
                text: this.msg(displayingTextProp)
            });
        } else {
            displayFailure(message);
        }
    };

    YAHOO.Bubbling.fire("registerAction",{ actionName: "onPublishMap",
        fn: function onPublishMap(record) {
            preloaderStart();
            Alfresco.util.Ajax.jsonGet({

                    url: Alfresco.constants.PROXY_URI + "nexperia/publishMap?nodeRef=" + record.nodeRef,

                    successCallback: {
                        fn: function onPublishMap_Success(response) {
                            preloaderStop();
                            YAHOO.Bubbling.fire("metadataRefresh");
                            if (response.serverResponse.responseText) {
                                var message = JSON.parse(response.serverResponse.responseText)["message"];
                                displayMessage.call(this, message, "onPublishMap.message.publish-map.success");
                            } else {
                                Alfresco.util.PopupManager.displayMessage({
                                    text: this.msg("onPublishMap.message.publish-map.success")
                                });
                            }
                        },
                        scope: this
                    },
                failureCallback: {
                    fn: function (response) {
                        preloaderStop();
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["message"];
                            displayFailure(message);
                        } else {
                            Alfresco.util.PopupManager.displayMessage({
                                text: this.msg("onPublishMap.message.publish-map.failure")
                            });
                        }
                    },
                    scope: this
                }
            });

        }
    });

    YAHOO.Bubbling.fire("registerAction",{ actionName: "onPublishTopic",
        fn: function onPublishTopic(record) {
            preloaderStart();
            Alfresco.util.Ajax.jsonGet({
                url: Alfresco.constants.PROXY_URI + "nexperia/publishTopic?nodeRef=" + record.nodeRef,
                successCallback: {
                    fn: function onPublishTopic_Success(response) {
                        preloaderStop();
                        YAHOO.Bubbling.fire("metadataRefresh");
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["message"];
                            displayMessage.call(this, message, "onPublishTopic.message.publish-topic.success");
                        } else {
                            Alfresco.util.PopupManager.displayMessage({
                                text: this.msg("onPublishTopic.message.publish-topic.success")
                            });
                        }
                    },
                    scope: this
                },
                failureCallback: {
                    fn: function (response) {
                        preloaderStop();
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["message"];
                            displayFailure(message);
                        } else {
                            Alfresco.util.PopupManager.displayMessage({
                                text: this.msg("onPublishTopic.message.publish-topic.failure")
                            });
                        }
                    },
                    scope: this
                }
            });
        }
    });

})();
