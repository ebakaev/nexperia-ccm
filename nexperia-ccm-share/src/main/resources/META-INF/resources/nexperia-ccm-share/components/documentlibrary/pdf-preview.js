(function () {
    YAHOO.Bubbling.fire("registerAction", {
        actionName: "ditaToPreview",
        fn: function ditamap2pdf(record) {
            var toPdfUrl;
            if (Alfresco.constants.PROXY_URI.indexOf("qa.") !== -1){
                toPdfUrl = "https://qa.eptos.pim.ssg.nexperia.com/ssg/map/ccmshare.pdf?type=ccmshare&custom="+record.webdavUrl.replace("/webdav/","")+"&nodeRef="+record.nodeRef+"&purgecache=yes&user=ssg&password=%40Nex17%23peria";
            } else {
                toPdfUrl = "https://www.eptos.pim.ssg.nexperia.com/ssg/map/ccmshare.pdf?type=ccmshare&custom="+record.webdavUrl.replace("/webdav/","")+"&nodeRef="+record.nodeRef+"&purgecache=yes&user=ssg&password=LRS%26Rq27PRnr4p%21L";
            }
            window.open(toPdfUrl, "_blank");
        }
    });
})();
