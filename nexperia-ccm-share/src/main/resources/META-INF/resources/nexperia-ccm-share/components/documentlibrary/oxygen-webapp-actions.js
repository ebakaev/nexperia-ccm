(function() {

  YAHOO.Bubbling.fire("registerAction", {
    actionName : "onActionAuthorOxygenWebapp",
    fn : function(record)
    {
  		var authUrl = Alfresco.constants.PROXY_URI + 'componize/authentication/ticket';
  		$.ajax(authUrl).done(function(ticket) {

        var webdavUrl = record.webdavUrl.replace('webdav/', '');

        if (record.workingCopy && record.workingCopy.isWorkingCopy) {
          webdavUrl = webdavUrl.substr(0, webdavUrl.lastIndexOf("/") + 1) + encodeURIComponent(record.displayName);
        }

        var cmisUrl = 'cmis://' + window.location.hostname + webdavUrl;

        if (!window.location.origin) { // Some browsers (mainly IE) does not have this property, so we need to build it manually...
          window.location.origin = window.location.protocol + '//' + window.location.hostname + (window.location.port ? (':' + window.location.port) : '');
        }

	    	window.open(window.location.origin + '/oxygen/app/oxygen.html?' +
          'url=' + encodeURIComponent(cmisUrl) +
          '&userName=' + Alfresco.constants.USERNAME +
          '&alf_ticket=' + ticket, '_blank');
  		});
    }
  });

})();
