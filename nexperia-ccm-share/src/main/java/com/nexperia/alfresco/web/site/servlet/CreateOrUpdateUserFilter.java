package com.nexperia.alfresco.web.site.servlet;

import org.alfresco.error.AlfrescoRuntimeException;
import org.springframework.context.ApplicationContext;
import org.springframework.extensions.surf.exception.ConnectorServiceException;
import org.springframework.extensions.surf.util.URLEncoder;
import org.springframework.extensions.webscripts.connector.Connector;
import org.springframework.extensions.webscripts.connector.ConnectorContext;
import org.springframework.extensions.webscripts.connector.ConnectorService;
import org.springframework.extensions.webscripts.connector.HttpMethod;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CreateOrUpdateUserFilter implements Filter {

    private ConnectorService connectorService;
    private Boolean createUsersInGroups;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
        this.connectorService = (ConnectorService) context.getBean("connector.service");
        this.createUsersInGroups = Boolean.parseBoolean(filterConfig.getInitParameter("createUsersInGroups"));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();

        try {
            if (createUsersInGroups) {
                if (session.getAttribute(UserCredentials.FIRST_LOGIN.attribute) != null) {
                    Connector connector = connectorService.
                            getConnector("alfresco-noauth", (String) session.getAttribute(UserCredentials.EMAIL.attribute), session);

                    connector.call("/update/user/info" +
                                    "?firstName=" + URLEncoder.encode((String) session.getAttribute(UserCredentials.FIRST_NAME.attribute)) +
                                    "&lastName=" + URLEncoder.encode((String) session.getAttribute(UserCredentials.LAST_NAME.attribute)) +
                                    "&nickName=" + URLEncoder.encode((String) session.getAttribute(UserCredentials.NICK_NAME.attribute)) +
                                    "&email=" + URLEncoder.encode((String) session.getAttribute(UserCredentials.EMAIL.attribute)) +
                                    "&groups=" + URLEncoder.encode((String) session.getAttribute(UserCredentials.GROUPS.attribute)),

                            new ConnectorContext(HttpMethod.POST));

                    session.removeAttribute(UserCredentials.FIRST_LOGIN.attribute);
                }
            }
            chain.doFilter(request, response);

        } catch (ConnectorServiceException e) {
            throw new AlfrescoRuntimeException(e.getLocalizedMessage());
        }
    }


    @Override
    public void destroy() {

    }
}