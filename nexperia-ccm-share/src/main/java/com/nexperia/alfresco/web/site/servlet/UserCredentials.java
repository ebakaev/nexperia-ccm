package com.nexperia.alfresco.web.site.servlet;

public enum UserCredentials {
    FIRST_NAME("givenname"),
    LAST_NAME("surname"),
    NICK_NAME("displayname"),
    EMAIL("emailaddress"),
    FIRST_LOGIN("firstLogin"),
    GROUPS("groups");

    String attribute;

    UserCredentials(String attribute) {
        this.attribute = attribute;
    }
}
