<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="no"
                doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">
        <html>
            <xsl:attribute name="rev">
                <xsl:value-of select="topic/@rev"/>
            </xsl:attribute>
            <xsl:attribute name="xml:lang">en-US</xsl:attribute>
            <xsl:attribute name="id">
                <xsl:value-of select="topic/@id"/>
            </xsl:attribute>
            <head/>
            <body>
                <h2>
                    <xsl:value-of select="topic/title"/>
                </h2>
                <div>
                    <xsl:variable name="countOfP" select="count(topic/body/ul/li/p)"/>
                    <xsl:variable name="countOfP2" select="count(topic/body/p)"/>
                    <xsl:choose>
                        <xsl:when test="$countOfP = 0">
                            <xsl:for-each select="topic/body/p">
                                <p>
                                    <xsl:apply-templates/>
                                </p>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <ul class="ul">
                                <xsl:for-each select="topic/body/ul/li">
                                    <li class="li">
                                        <xsl:apply-templates/>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="topic/body/ul/li/p | topic/body/ul/li/ul/li/p">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="topic/body/ul/li/ul">
        <ul class="ul">
            <xsl:for-each select="li">
                <li class="li">
                    <xsl:apply-templates/>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="topic/body/ul/li/p/sub | topic/body/ul/li/ul/li/p/sub">
        <sub class="ph sub">
            <xsl:value-of select="."/>
        </sub>
    </xsl:template>

    <xsl:template match="topic/body/p/sub">
        <sub class="ph sub">
            <xsl:value-of select="text()"/>
        </sub>
    </xsl:template>

    <xsl:template match="topic/body/p/overline">
        <span style="text-decoration: overline">
            <xsl:value-of select="text()"/>
        </span>
    </xsl:template>

    <xsl:template name="string-replace-all">
        <xsl:param name="text"/>
        <xsl:param name="replace"/>
        <xsl:param name="by"/>
        <xsl:choose>
            <xsl:when test="$text = '' or $replace = '' or not($replace)">
                <!-- Prevent this routine from hanging -->
                <xsl:value-of select="$text"/>
            </xsl:when>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)"/>
                <xsl:value-of select="$by"/>
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="substring-after($text,$replace)"/>
                    <xsl:with-param name="replace" select="$replace"/>
                    <xsl:with-param name="by" select="$by"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
