<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="no"
                doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

    <xsl:template match="/">
        <html>
            <xsl:attribute name="rev"><xsl:value-of select="map/@rev"/></xsl:attribute>
            <xsl:attribute name="xml:lang">en-US</xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="map/@id"/></xsl:attribute>
            <head>
                <title>
                    <xsl:value-of select="map/topicmeta/shortdesc"/>
                </title>
                <xsl:for-each select="map/topicmeta/keywords/keyword">
                    <meta>
                        <xsl:attribute name="name">keywords</xsl:attribute>
                        <xsl:attribute name="content"><xsl:value-of select="."/></xsl:attribute>
                    </meta>
                </xsl:for-each>
                <xsl:for-each select="map/topicmeta/othermeta">
                    <meta>
                        <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
                        <xsl:attribute name="content"><xsl:value-of select="@content"/></xsl:attribute>
                    </meta>
                </xsl:for-each>
                <meta>
                    <xsl:attribute name="name">golive</xsl:attribute>
                    <xsl:attribute name="content"><xsl:value-of select="map/topicmeta/critdates/revised/@golive"/></xsl:attribute>
                </meta>
            </head>
            <body>
                <ul>
                    <xsl:for-each select="map/topicref">
                        <xsl:variable name="updatedHref">
                            <xsl:call-template name="string-replace-all">
                                <xsl:with-param name="text" select="@href" />
                                <xsl:with-param name="replace" select="'dita'" />
                                <xsl:with-param name="by" select="'html'" />
                            </xsl:call-template>
                        </xsl:variable>
                        <li>
                            <a>
                                <xsl:attribute name="href"><xsl:value-of select="$updatedHref"/></xsl:attribute>
                                <xsl:attribute name="name"><xsl:call-template name="ditaref" /></xsl:attribute>
                                <xsl:attribute name="id"><xsl:call-template name="ditaref" /></xsl:attribute>
                                <xsl:call-template name="ditalable" />
                            </a>
                        </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="ditalable">
        <xsl:choose>
            <xsl:when test="contains(@href, 'rh_')">Revision history</xsl:when>
            <xsl:when test="contains(@href, 'hl_')">Headline</xsl:when>
            <xsl:when test="contains(@href, 'ts_')">General description</xsl:when>
            <xsl:when test="contains(@href, 'ms_')">Marketing summary</xsl:when>
            <xsl:when test="contains(@href, 'fb_')">Features and benefits</xsl:when>
            <xsl:when test="contains(@href, 'ta_')">Applications</xsl:when>
            <xsl:when test="contains(@href, 'ap_')">About product line</xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="ditaref">
        <xsl:choose>
            <xsl:when test="contains(@href, 'rh_')">revision-history-ref</xsl:when>
            <xsl:when test="contains(@href, 'hl_')">headline-ref</xsl:when>
            <xsl:when test="contains(@href, 'ts_')">technical-summary-ref</xsl:when>
            <xsl:when test="contains(@href, 'ms_')">marketing-summary-ref</xsl:when>
            <xsl:when test="contains(@href, 'fb_')">features-benefits-ref</xsl:when>
            <xsl:when test="contains(@href, 'ta_')">target-applications-ref</xsl:when>
            <xsl:when test="contains(@href, 'ap_')">about-pl-ref</xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="string-replace-all">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="$text = '' or $replace = '' or not($replace)" >
                <!-- Prevent this routine from hanging -->
                <xsl:value-of select="$text" />
            </xsl:when>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
