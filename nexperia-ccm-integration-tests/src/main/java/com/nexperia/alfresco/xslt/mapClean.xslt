<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ditaarch="http://dita.oasis-open.org/architecture/2005/">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="no"
                doctype-public="-//OASIS//DTD DITA 1.3 Map//EN"
                doctype-system="http://www.data.nexperia.com/schemas/dita/v1.3/dtd/technicalContent/dtd/map.dtd"/>

    <xsl:template match="@class"/>
    <xsl:template match="@domains"/>
    <xsl:template match="@ditaarch:DITAArchVersion"/>

	<xsl:template match="@*|node()">
		<xsl:copy copy-namespaces="no">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="map">
		<xsl:copy copy-namespaces="no">
			<xsl:apply-templates select="@*|node()"/>
			<xsl:attribute name="xml:lang">en-US</xsl:attribute>
		</xsl:copy>
	</xsl:template>

    <xsl:template match="topicref">
        <xsl:copy copy-namespaces="no">
                        <xsl:apply-templates select="@*"/>
            <xsl:attribute name="href">
                <xsl:text>topics/</xsl:text>
                <xsl:value-of select="substring-after(@href, 'topics/')"/>
            </xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="ditavalref">
        <xsl:copy copy-namespaces="no">
            <xsl:attribute name="format">
                <xsl:value-of select="@format"/>
            </xsl:attribute>
            <xsl:attribute name="href">
                <xsl:value-of select="@href"/>
            </xsl:attribute>
            <xsl:attribute name="processing-role">
                <xsl:value-of select="@processing-role"/>
            </xsl:attribute>
            <xsl:attribute name="rev">
                <xsl:value-of select="@rev"/>
            </xsl:attribute>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>