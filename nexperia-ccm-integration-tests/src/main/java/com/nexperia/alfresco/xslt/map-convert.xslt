<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="fn xs">
 <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"
		doctype-public="-//OASIS//DTD DITA 1.3 Map//EN"
		doctype-system="http://www.data.nexperia.com/schemas/dita/v1.3/dtd/technicalContent/dtd/map.dtd"/>

	<xsl:variable name="keywords" select="value-proposition/topicmeta/keywords"></xsl:variable>
	<xsl:variable name="mainvisualcontent" select="normalize-space(value-proposition/topicmeta/mainvisual/@content)"></xsl:variable>

	<xsl:template match="@class" />
	<xsl:template match="@title" />
	<xsl:template match="publisher" />
	<xsl:template match="subtitle" />
	<xsl:template match="nxp-type" />
	<xsl:template match="specificationstate" />
	<xsl:template match="mainvisual" />
	<xsl:template match="shortdesc" />
	<xsl:template match="releasestate" />
	<xsl:template match="owner" />
	<xsl:template match="magcode" />
	<xsl:template match="source" />
	<xsl:template match="code" />

    <xsl:template match="value-proposition">
      <map>
        <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
		<xsl:attribute name="rev"><xsl:value-of select="@rev"/></xsl:attribute>
		<xsl:attribute name="xml:lang"><xsl:text>en-US</xsl:text></xsl:attribute>
        <title>
            <xsl:value-of select="/value-proposition/@title"/>
        </title>
        <xsl:apply-templates/>
      </map>
    </xsl:template>

	<xsl:template match="value-proposition/topicmeta">
		<topicmeta>
 		    <shortdesc><xsl:value-of select="subtitle"/></shortdesc>
 		    <publisher format="html" href="https://www.nexperia.com/" scope="external">Nexperia BV</publisher>
  		    <critdates>
  		    	<revised>
					<xsl:attribute name="golive"><xsl:value-of select="critdates/revised/@golive"/></xsl:attribute>
					<xsl:attribute name="modified"><xsl:value-of select="critdates/revised/@modified"/></xsl:attribute>
  		    	</revised>	
  		    </critdates>
  		    <xsl:copy-of select="permissions"/>
   		    <xsl:copy-of select="audience"/>
   		    <xsl:copy-of select="$keywords" />
			<othermeta>
				<xsl:attribute name="name"><xsl:text>type</xsl:text></xsl:attribute>
				<xsl:attribute name="content"><xsl:value-of select="nxp-type/@content"/></xsl:attribute>
			</othermeta>
			<othermeta>
				<xsl:attribute name="name"><xsl:text>maturity</xsl:text></xsl:attribute>
				<xsl:attribute name="content"><xsl:value-of select="specificationstate/@content"/></xsl:attribute>
			</othermeta>
			<othermeta>
				<xsl:attribute name="name"><xsl:text>code</xsl:text></xsl:attribute>
				<xsl:attribute name="content"><xsl:value-of select="code/@content"/></xsl:attribute>
			</othermeta>
			<othermeta>
				<xsl:attribute name="name"><xsl:text>magcode</xsl:text></xsl:attribute>
				<xsl:attribute name="content"><xsl:value-of select="magcode/@content"/></xsl:attribute>
			</othermeta>
			<othermeta>
				<xsl:attribute name="name"><xsl:text>owner</xsl:text></xsl:attribute>
				<xsl:attribute name="content"><xsl:value-of select="owner/@content"/></xsl:attribute>
			</othermeta>
			<othermeta>
				<xsl:attribute name="name"><xsl:text>source</xsl:text></xsl:attribute>
				<xsl:attribute name="content"><xsl:text>CCMShare</xsl:text></xsl:attribute>
			</othermeta>
            <data name="main-visual">
				<xsl:choose>
					<xsl:when test="$mainvisualcontent = ''">
                        <image>
								<xsl:attribute name="href">https://assets.nexperia.com/documents/logo/Nexperia_X_RGB.svg</xsl:attribute>								
						</image>
					</xsl:when>
					<xsl:otherwise>
                        <image>
							<xsl:attribute name="href"><xsl:value-of select="mainvisual/@content"/></xsl:attribute>
						</image>
					</xsl:otherwise>
			   </xsl:choose>
            </data>
		</topicmeta>
	</xsl:template>

	<xsl:template match="*[contains(@href, 'hl_')] | *[contains(@href, 'ts_')] | *[contains(@href, 'ms_')] | *[contains(@href, 'fb_')] | *[contains(@href, 'ta_')] | *[contains(@href, 'ap_')]" name="references">
		<xsl:choose>
			<xsl:when test="contains(@href, 'hl_')">
				<topicref>
					<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
				</topicref>
			</xsl:when>
			<xsl:when test="contains(@href, 'ts_')">
				<topicref>
					<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
				</topicref>
			</xsl:when>
			<xsl:when test="contains(@href, 'ms_')">
				<topicref>
					<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
				</topicref>
			</xsl:when>
			<xsl:when test="contains(@href, 'fb_')">
				<topicref>
					<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
				</topicref>
			</xsl:when>
			<xsl:when test="contains(@href, 'ta_')">
				<topicref>
					<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
				</topicref>
			</xsl:when>
			<xsl:when test="contains(@href, 'ap_')">
				<topicref>
					<xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute>
				</topicref>
			</xsl:when>
		</xsl:choose>
	</xsl:template>   

    <xsl:template match="@*|node()">
        <xsl:copy copy-namespaces="no">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template> 
   
</xsl:stylesheet>