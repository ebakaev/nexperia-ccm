<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                version="2.0">

    <xsl:template match="xhtml:meta"/>
    <xsl:template match="xhtml:link"/>
    <xsl:template match="xhtml:colgroup"/>
    <xsl:template match="@cellpadding"/>
    <xsl:template match="@cellspacing"/>
    <xsl:template match="@summary"/>
    <xsl:template match="@class"/>
    <xsl:template match="@frame"/>
    <xsl:template match="@border"/>
    <xsl:template match="@rules"/>
    <xsl:template match="@style"/>

    <xsl:template match="xhtml:table" name="wrapTable">
        <div class="table-responsive">
            <table class="table table-products">
                <xsl:apply-templates select="node()|@*"/>
            </table>
        </div>
    </xsl:template>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>