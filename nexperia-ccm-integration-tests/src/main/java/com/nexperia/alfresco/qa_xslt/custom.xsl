<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    version="2.0">

    <xsl:template match="xhtml:meta"/>
    <!-- <xsl:template match="xhtml:meta[@content='text/html; charset=UTF-8']"/> -->
    <xsl:template match="xhtml:link"/>
    <xsl:template match="xhtml:colgroup"/>
    <xsl:template match="@cellpadding"/>
    <xsl:template match="@cellspacing"/>
    <xsl:template match="@summary"/>
    <xsl:template match="@class"/>
    <xsl:template match="@frame"/>
    <xsl:template match="@border"/>
    <xsl:template match="@rules"/>
    <xsl:template match="@style"/>

    <!-- Change h1 elements into h2 elements -->
    <xsl:template match="*[contains(@class, ' topic/title ')]">
        <h2 class="title topictitle2">
            <xsl:call-template name="gen-user-panel-title-pfx"/>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>
    
    <!-- Move figure title and description to bottom within Figure caption -->
    <xsl:template match="*[contains(@class, ' topic/fig ')]" name="topic.fig.nexperia">
        <figure>
            <xsl:call-template name="setidaname"/>
            <xsl:apply-templates select="*[not(contains(@class,' topic/title ') or contains(@class,' topic/desc '))]"/>
            <xsl:call-template name="place-fig-lbl.nexperia"/>
        </figure>
    </xsl:template>
    
    <xsl:template name="place-fig-lbl.nexperia">
        <xsl:param name="stringName"/>
        <!-- Number of fig/title's including this one -->
        <xsl:variable name="fig-count-actual" select="count(preceding::*[contains(@class, ' topic/fig ')]/*[contains(@class, ' topic/title ')])+1"/>
        <xsl:variable name="ancestorlang">
            <xsl:call-template name="getLowerCaseLang"/>
        </xsl:variable>
        <xsl:choose>
            <!-- title -or- title & desc -->
            <xsl:when test="*[contains(@class, ' topic/title ')]">
                <figcaption>
                    <xsl:if test="*[contains(@class, ' topic/desc ')]">
                        <xsl:text>. </xsl:text>
                    </xsl:if>
                    <xsl:for-each select="*[contains(@class, ' topic/desc ')]">
                        <span class="figdesc">
                            <xsl:call-template name="commonattributes"/>
                            <xsl:apply-templates select="." mode="figdesc"/>
                        </span>
                    </xsl:for-each>
                    <span class="fig--title-label">
                        <xsl:choose>      <!-- Hungarian: "1. Figure " -->
                            <xsl:when test="$ancestorlang = ('hu', 'hu-hu')">
                                <xsl:value-of select="$fig-count-actual"/>
                                <xsl:text>. </xsl:text>
                                <xsl:call-template name="getVariable">
                                    <xsl:with-param name="id" select="'Figure'"/>
                                </xsl:call-template>
                                <xsl:text> </xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:call-template name="getVariable">
                                    <xsl:with-param name="id" select="'Figure'"/>
                                </xsl:call-template>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="$fig-count-actual"/>
                                <xsl:text>. </xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </span>
                    <xsl:apply-templates select="*[contains(@class, ' topic/title ')]" mode="figtitle"/>
                </figcaption>
            </xsl:when>
            <!-- desc -->
            <xsl:when test="*[contains(@class, ' topic/desc ')]">
                <xsl:for-each select="*[contains(@class, ' topic/desc ')]">
                    <figcaption>
                        <xsl:call-template name="commonattributes"/>
                        <xsl:apply-templates select="." mode="figdesc"/>
                    </figcaption>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>