<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    
    <xsl:template match="/">
        <html>
            <xsl:apply-templates/>    
        </html>
    </xsl:template>
    
    <xsl:template match="p">
        <p class="p">Eduard rewrites P tag</p>
    </xsl:template>
    
</xsl:stylesheet>