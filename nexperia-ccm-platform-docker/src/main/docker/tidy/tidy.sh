#!/bin/bash
tidy -config /usr/local/tomcat/tidy/tidy-config-value-proposition.properties $1 | sed -e 's/\r\n//g' -e ':a;N;$! ba;s/\n//g' -e 's/lang="en-US"/ /' -e 's/lang="en-US"/ /' -e 's/xml:/xml:lang="en-US"/' -e 's#xmlns="http://www\.w3\.org/1999/xhtml"##' > $2
