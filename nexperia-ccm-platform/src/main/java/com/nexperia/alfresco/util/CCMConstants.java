package com.nexperia.alfresco.util;

import org.alfresco.service.namespace.QName;

public interface CCMConstants {

    public static final String CCM_NAMESPACE = "http://www.nexperia.com/ccm/content-model";

    public static final QName ASPECT_PACKING_INFORMATION = QName.createQName(CCM_NAMESPACE, "packingAspect");
    public static final QName ASPECT_VALUE_PROPOSITION = QName.createQName(CCM_NAMESPACE, "vpAspect");
    public static final QName ASPECT_DATA_SHEET = QName.createQName(CCM_NAMESPACE, "dsAspect");
    public static final QName ASPECT_APPLICATION_NOTE = QName.createQName(CCM_NAMESPACE, "appNoteAspect");
    public static final QName ASPECT_SUBJECT_SCHEME = QName.createQName(CCM_NAMESPACE, "subjectSchemeAspect");
    public static final String CONTENT_DOCUMENT_TYPE_PACKING = "Packing information";
    public static final String DOCUMENT_TYPE_VALUE_PROPOSITION = "Value proposition";
    public static final String DOCUMENT_TYPE_DATA_SHEET = "Data sheet";
    public static final String DOCUMENT_TYPE_PACKING_INFORMATION = "Packing information";
    public static final String DOCUMENT_TYPE_APPLICATION_NOTE = "Application note";
    public static final String DOCUMENT_TYPE_SUBJECT_SCHEME = "Subject Scheme";
    public static final String WRONG_DOCUMENT_TYPE_VALUE_PROPOSITION = "value proposition";
    public static final String PUBLICATION_TYPE_TAG = "publication-type";
    public static final String SUBJECT_SCHEME_TAG = "subjectScheme";
    public static final String TOPIC_TAG = "<topic";
    public static final String DOC_VP_TAG = "name=\"doc-identifier\"";
    public static final String MAP_TAG = "<map";
    public static final String REV_ATTR = "rev=";
    public static final String ID_ATTR = "id=";
    public static final String MAP_REF_TAG = "<mapref";
    public static final String MAP_REF_FORMAT_TAG = "format=\"ditamap\"";
    public static final String MAP_REF_VALUE_START = "href=\"";
    public static final String MAP_REF_VALUE_END = "\"";


    static final String CONTENT_DITAVAL_NAME = "Automotive.ditaval";
    static final String CONTENT_NON_DITAVAL_NAME = "Non-Automotive.ditaval";

    static final String SECURITY_STATUS_COMPANY_PUBLIC = "Company Public";
    static final String STATUS_RELEASED = "Released";

    static final String DOCUMENT_TYPE_VP = "Value proposition";
    static final String DOCUMENT_TYPE_DS = "Data sheet";
    static final String DOCUMENT_TYPE_TOPIC = "topic";


    // Legacy aspects
    public static final String XML_CM_NAMESPACE = "http://www.nxp.com/xmlcm/content-model";
    public static final QName ASPECT_LEGACY_PACKING_INFORMATION = QName.createQName(XML_CM_NAMESPACE, "packingAspect");
    public static final QName ASPECT_LEGACY_VALUE_PROPOSITION = QName.createQName(XML_CM_NAMESPACE, "vpAspect");
    public static final QName ASPECT_LEGACY_DATA_SHEET = QName.createQName(XML_CM_NAMESPACE, "dsAspect");
    public static final QName ASPECT_LEGACY_APPLICATION_NOTE = QName.createQName(XML_CM_NAMESPACE, "appNoteAspect");
    public static final QName ASPECT_LEGACY_SUBJECT_SCHEME = QName.createQName(XML_CM_NAMESPACE, "subjectSchemeAspect");

}
