package com.nexperia.alfresco.util;

import com.nexperia.alfresco.service.TidyService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.version.Version2Model;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Util {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    ServiceRegistry serviceRegistry;
    NodeService nodeService;
    StoreRef storeUrl;
    private String updateTopicTransformXsltPath;
    private String checkTopicTransformXsltPath;
    private String cleanUpHtmlTransformXsltPath;

    public final static String VALID = "valid";
    public final static String INVALID = "invalid";
    public final static String ERROR = "error";

    public String getName(NodeRef nodeRef) {
        return (String) nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public String getName(String nodeRef) {
        return getName(new NodeRef(nodeRef));
    }

    public boolean isDataSheet(String nodeRef) {
        return existsAndHasAspect(new NodeRef(nodeRef), CCMConstants.ASPECT_DATA_SHEET);
    }

    private boolean existsAndHasAspect(NodeRef nodeRef, QName aspect) {
        NodeService ns = serviceRegistry.getNodeService();
        if (nodeRef == null || !ns.exists(nodeRef)) {
            return false;
        }
        if (isVersionNode(nodeRef)) {
            NodeRef baseNodeRef = getBaseNode(nodeRef);
            if (baseNodeRef == null || !ns.exists(baseNodeRef)) {
                logger.warn("Base node for version node {} does not exist with nodeRef {}", nodeRef, baseNodeRef);
                return false;
            }
            nodeRef = baseNodeRef;
        }
        return ns.hasAspect(nodeRef, aspect);
    }

    public NodeRef getBaseNode(NodeRef baseOrVersion) {
        NodeRef realVersionRef = getWorkspaceStoreNode(baseOrVersion);
        if (ObjectUtils.notEqual(baseOrVersion, realVersionRef)) {
            return (NodeRef) serviceRegistry.getNodeService().getProperty(
                    realVersionRef, Version2Model.PROP_QNAME_FROZEN_NODE_REF);
        } else {
            return baseOrVersion;
        }
    }

    public static NodeRef getWorkspaceStoreNode(NodeRef baseOrVersion) {
        if (baseOrVersion == null) {
            return null;
        }
        if (!"versionStore".equals(baseOrVersion.getStoreRef().getProtocol())) {
            return baseOrVersion;
        }
        return new NodeRef(new StoreRef("workspace",
                baseOrVersion.getStoreRef().getIdentifier()), baseOrVersion.getId());
    }

    public NodeRef getNodeByPath(String containerPath) {
        NodeRef store = serviceRegistry.getNodeService().getRootNode(storeUrl);
        List<NodeRef> nodes = serviceRegistry.getSearchService().selectNodes(store, containerPath,
                null, serviceRegistry.getNamespaceService(), false);
        if (nodes != null && nodes.size() > 0) {
            return nodes.get(0);
        }
        return null;
    }

    public void setStoreUrl(StoreRef storeUrl) {
        this.storeUrl = storeUrl;
    }

    public void setUpdateTopicTransformXsltPath(String updateTopicTransformXsltPath) {
        this.updateTopicTransformXsltPath = updateTopicTransformXsltPath;
    }

    public void setCheckTopicTransformXsltPath(String checkTopicTransformXsltPath) {
        this.checkTopicTransformXsltPath = checkTopicTransformXsltPath;
    }

    public void setCleanUpHtmlTransformXsltPath(String cleanUpHtmlTransformXsltPath) {
        this.cleanUpHtmlTransformXsltPath = cleanUpHtmlTransformXsltPath;
    }

    public static boolean isVersionNode(NodeRef nodeRef) {
        return "versionStore".equals(nodeRef.getStoreRef().getProtocol());
    }

    public InputStream updateTopicTransform(String name, ContentReader cr) {
        NodeRef xslFile = null;
        if (name.contains(".dita")) {
            xslFile = getNodeByPath(updateTopicTransformXsltPath);
            TransformerFactory factory = TransformerFactory.newInstance();
            ContentReader xsltCR = serviceRegistry.getContentService().getReader(xslFile, ContentModel.PROP_CONTENT);
            Source xslt = new StreamSource(xsltCR.getContentInputStream());
            Transformer transformer = null;
            File tempFile = TempFileProvider.createTempFile("updated_ " + name, ".out");
            try {
                transformer = factory.newTransformer(xslt);
                Source source = new StreamSource(cr.getContentInputStream());
                Result resultOS = new StreamResult(new FileOutputStream(tempFile));
                transformer.transform(source, resultOS);
                return new FileInputStream(tempFile);
            } catch (Exception e) {
                logger.error("Topic '{}' update transformation filed", name, e);
                return null;
            }
        }

        return null;
    }

    public String checkTopicTransform(String name, ContentReader cr) {
        NodeRef xslFile = null;
        if (name.contains(".dita")) {
            xslFile = getNodeByPath(checkTopicTransformXsltPath);
            TransformerFactory factory = TransformerFactory.newInstance();
            ContentReader xsltCR = serviceRegistry.getContentService().getReader(xslFile, ContentModel.PROP_CONTENT);
            Source xslt = new StreamSource(xsltCR.getContentInputStream());
            Transformer transformer = null;
            File tempFile = TempFileProvider.createTempFile("updated_ " + name, ".out");
            try {
                transformer = factory.newTransformer(xslt);
                Source source = new StreamSource(cr.getContentInputStream());
                Result resultOS = new StreamResult(new FileOutputStream(tempFile));
                transformer.transform(source, resultOS);
                String fileContent = IOUtils.toString(new FileInputStream(tempFile), StandardCharsets.UTF_8);

                return fileContent.contains(INVALID) ? ERROR : VALID;
            } catch (Exception e) {
                logger.error("Topic '{}' check transformation filed", name, e);
                return VALID;
            }
        }

        return VALID;
    }

    public List<NodeRef> searchByQuery(String query) {
        ResultSet results = serviceRegistry.getSearchService().query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_FTS_ALFRESCO, query);
        List<NodeRef> result = results.getNodeRefs();
        results.close();
        return result;
    }

    public String format(String unformattedXml) {
        try {
            final Document document = parseXmlFile(unformattedXml);

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String format(InputStream unformattedXml) {
        try {
            final Document document = parseXmlFile(unformattedXml);

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(120);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Document parseXmlFile(String in) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(in));
            return db.parse(is);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Document parseXmlFile(InputStream in) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(in);
            return db.parse(is);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isTopicComplicated(NodeRef zipSource) {
        return true;
    }

    /**
     * This method zips the directory
     *
     * @param dir
     * @param zipDirName
     * @param tidyService
     */
    public void zipDirectory(File dir, String zipDirName, TidyService tidyService) {
        try {
            List<String> paths = getFilesPaths(dir, null);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (String filePath : paths) {
                logger.debug("Zipping {}", filePath);
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                File cleanedFile;
                if (filePath.contains(".html")) {
                    filePath = makeChangesOnTransformedContentXSLT(filePath);
                    cleanedFile = tidyService.clean(new File(filePath));
                } else {
                    cleanedFile = new File(filePath);
                }

                FileInputStream fis = new FileInputStream(cleanedFile);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String makeChangesOnTransformedContentXSLT(String filePath) {
        File contentToUpdate = new File(filePath);
        String name = contentToUpdate.getName();
        logger.debug("XSLT cleanup for {} [{}]", contentToUpdate.getName(), contentToUpdate.getAbsolutePath());
        NodeRef xslFile = null;

        xslFile = getNodeByPath(cleanUpHtmlTransformXsltPath);
        TransformerFactory factory = TransformerFactory.newInstance();
        ContentReader xsltCR = serviceRegistry.getContentService().getReader(xslFile, ContentModel.PROP_CONTENT);
        Source xslt = new StreamSource(xsltCR.getContentInputStream());
        Transformer transformer = null;
        File tempFile = TempFileProvider.createTempFile(UUID.randomUUID() +
                "_cleaned_up_html_" + FilenameUtils.getBaseName(name), ".out");
        logger.debug("Created temporary file [{}]", tempFile.getAbsolutePath());
        try {
            transformer = factory.newTransformer(xslt);
            Source source = new StreamSource(new FileInputStream(contentToUpdate));
            Result resultOS = new StreamResult(new FileOutputStream(tempFile));
            transformer.transform(source, resultOS);
            logger.debug("Return cleaned up temp file [{}]", tempFile.getAbsolutePath());
            return tempFile.getAbsolutePath();
        } catch (Exception e) {
            logger.error("Html '{}' clean up transformation filed", name, e);
            return null;
        }
    }

    private String makeChangesOnTranformedContent(String filePath) {
        File contentToUpdate = new File(filePath);
        org.jsoup.nodes.Document document = null;
        try {
            document = Jsoup.parse(contentToUpdate, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements elements = document.select("h1");

        int count = 0;
        for (Element element : elements) {
            if (count < 1) {
                element.tagName("h2");
            }
            count++;
        }

        File updatedHtml = TempFileProvider.createTempFile((new Date()).getTime() + "_", FilenameUtils.getExtension(contentToUpdate.getName()));
        try {
            FileUtils.writeStringToFile(updatedHtml, document.html(), Charset.forName("UTF-8"));
            return updatedHtml.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * This method populates all the files in a directory to a List
     *
     * @param dir
     * @throws IOException
     */
    public List<String> getFilesPaths(File dir, List<String> paths) throws IOException {
        if (paths == null) paths = new ArrayList<>();
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) paths.add(file.getAbsolutePath());
            else getFilesPaths(file, paths);
        }
        return paths;
    }
}
