package com.nexperia.alfresco.remote.publication;

/**
 * This interface contains keys of configuration parameters 
 * used by publication client users to configure publication.
 * 
 * @author byaminov
 *
 */
public interface PublicationParameters {

	/**
	 * Boolean property. If value is "true", then publication possibility will be checked,
	 * but changes to Publication Space won't be committed.
	 */
	static final String PARAM_DRY_RUN = "dryrun";
	
	/**
	 * Boolean property. If value is "true", then in case there are superseded documents with
	 * greater version, a {@link SupersededVersionIsGreaterException} error won't be thrown, and
	 * document will be published.
	 */
	static final String PARAM_IGNORE_SUPERSEDED_VERSION = "ignoreSsVersion";
	
	/**
	 * Boolean property. If value is "true", then in case there are superseded documents with
	 * greater version, a {@link SupersededVersionIsGreaterException} error won't be thrown, and
	 * document will be published.
	 */
	static final String PARAM_SKIP_VERSION_INCREASE_CHECK = "ignoreVersionIncrease";

	/**
	 * Boolean property. If value is "true", then publication code will be run in the same transaction,
	 * as calling code, which can be useful only when running via 
	 * {@link LocalPublicationClientImpl}. By default publication
	 * is run in a separate non-propagating transaction.
	 */
	static final String PARAM_USE_SAME_TRANSACTION = "useSameTransaction";

}
