package com.nexperia.alfresco.remote.client;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.springframework.extensions.webscripts.connector.ResponseStatus;

/**
 * Replacement for {@link org.springframework.extensions.webscripts.connector.Response} to let create new
 * responses outside the org.alfresco.connector package.
 * 
 * @author byaminov
 * 
 */
public class Response {
	
	private String data;
	private InputStream is;
	private ResponseStatus status;
	private String encoding = null;

	public Response(String data, InputStream is, ResponseStatus status, String encoding) {
		this.data = data;
		this.is = is;
		this.status = status;
		this.encoding = encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getResponse() {
		return this.data;
	}

	public InputStream getResponseStream() {
		try {
			return (this.is != null ? this.is : new ByteArrayInputStream(
					this.data.getBytes(encoding)));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("UnsupportedEncodingException: "
					+ encoding);
		}
	}

	public ResponseStatus getStatus() {
		return this.status;
	}

	public String getEncoding() {
		return this.encoding;
	}

	@Override
	public String toString() {
		return this.data;
	}
}
