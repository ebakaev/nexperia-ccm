package com.nexperia.alfresco.remote.exception;




/**
 * Exception thrown when a document with id provided in update request 
 * does not exist in remote Publication space.
 * 
 * @author byaminov
 *
 */
public class NoSuchPublishedIdException extends WebScriptClientException {

	private static final long serialVersionUID = -6323653740588185546L;

	public NoSuchPublishedIdException(String message) {
		super(message);
	}

}
