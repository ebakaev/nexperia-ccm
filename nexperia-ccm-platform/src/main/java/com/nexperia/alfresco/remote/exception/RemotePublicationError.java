package com.nexperia.alfresco.remote.exception;

import com.nexperia.alfresco.remote.publication.InternalParameterConstants;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Enumeration of errors returned by {@link RemotePublicationWebscript}
 * @author byaminov
 *
 */
public enum RemotePublicationError  {
	
	OTHER(1, "Other error", WebScriptClientException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new WebScriptClientException(getMessage(params));
		}
	},
	
	ILLEGAL_ARGUMENT(2, "Bad request", IllegalParametersException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new IllegalParametersException(getMessage(params));
		}
		
		@Override
		public int getHttpStatus() {
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	},
	
	PUBLICATION_FORBIDDEN(3, "Forbidden for publication", PublicationForbiddenException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new PublicationForbiddenException(getMessage(params));
		}
		
		@Override
		public int getHttpStatus() {
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	},
	
	MISSING_MANDATORY_PROPERTIES(4, "Missing mandatory meta data", MissingMandatoryPropertiesException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			List<String> missingProps = null;
			Object param = params.get(InternalParameterConstants.KEY_MISSING_PROPERTIES);
			if (param != null && param instanceof String[]) {
				missingProps = new ArrayList<String>();
				for (String prop : (String[]) param) {
					missingProps.add(prop);
				}
			}
			return new MissingMandatoryPropertiesException(getMessage(params), missingProps);
		}
		
		@Override
		public int getHttpStatus() {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		@Override
		public void putCustomProperties(WebScriptClientException e, Map<String, Object> model) {
			if (e instanceof MissingMandatoryPropertiesException) {
				List<String> missingProperties = ((MissingMandatoryPropertiesException) e).
						getMissingProperties();
				if (missingProperties != null && missingProperties.size() > 0) {
					String[] missingProps = missingProperties.toArray(new String[missingProperties.size()]);
					model.put(InternalParameterConstants.KEY_MISSING_PROPERTIES, missingProps);
				}
			}
		}
	},
	
	FTP_FAILURE(5, "FTP failure", RemoteFtpException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new RemoteFtpException(getMessage(params));
		}
	},
	
	NO_PUBLICATION_SPACE(6, "No Publication space", NoRemotePublicationSpaceException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new NoRemotePublicationSpaceException(getMessage(params));
		}
	},
	
	FILE_EXISTS(7, "File already exists", RemoteFileExistsException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new RemoteFileExistsException(getMessage(params));
		}
	},
	
	PUBLISHED_NOT_EXISTS(8, "No published document with specified id", NoSuchPublishedIdException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new NoSuchPublishedIdException(getMessage(params));
		}
		
		@Override
		public int getHttpStatus() {
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	},
	
	NO_VERSION_INCREASE(10, "Published document version was not increased", NoVersionIncreaseException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new NoVersionIncreaseException(getMessage(params));
		}
		
		@Override
		public int getHttpStatus() {
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	},
	
	SUPERSEDED_VERSION_GREATER(11, "Superseded document version is greater than superseding one", 
			SupersededVersionIsGreaterException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new SupersededVersionIsGreaterException(getMessage(params));
		}
		
		@Override
		public int getHttpStatus() {
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	},
	
	EMPTY_CONTENT(12, "Content is empty", EmptyContentException.class) {
		@Override
		public WebScriptClientException getPublicationException(Map<String, Object> params) {
			return new EmptyContentException(getMessage(params));
		}
	};
	
	private final int code;
	private final String meaning;
	private List<Class<? extends WebScriptClientException>> relatedExceptions = 
		new ArrayList<Class<? extends WebScriptClientException>>();

	@SuppressWarnings("unchecked")
	RemotePublicationError(int code, String meaning, Class[] relatedExceptions) {
		this.code = code;
		this.meaning = meaning;
		for (Class clazz : relatedExceptions) {
			this.relatedExceptions.add(clazz);
		}
	}
	
	RemotePublicationError(int code, String meaning, 
			Class<? extends WebScriptClientException> relatedException) {
		this(code, meaning, new Class[] {relatedException});
	}
	
	/**
	 * Returns error code
	 * @return error code
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * Returns verbal description of the error code
	 * @return meaning of the code
	 */
	public String getMeaning() {
		return meaning;
	}
	
	/**
	 * Generates a publication exception corresponding to the
	 * meaning of the error
	 * @param params all parameters got from response. Each parameter
	 * value is whether a String or String[].
	 * @return WebScriptClientException or its inheritors, corresponding to the error
	 */
	public abstract WebScriptClientException getPublicationException(Map<String, Object> params);

	/**
	 * Gets list of exception classes which relate to this error.
	 * @return list of related exceptions
	 */
	public List<Class<? extends WebScriptClientException>> getRelatedExceptions() {
		return relatedExceptions;
	}
	
	/**
	 * Gets HTTP status which should be returned in case of this error.
	 * By default it is 500 (Internal Error), but errors can override this.
	 * @return HTTP status to set to response in case of this error.
	 */
	public int getHttpStatus() {
		return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
	}

	/**
	 * Puts custom properties into the result model
	 * to be sent back to client.
	 * @param e publication exception
	 * @param model model for template
	 */
	public void putCustomProperties(WebScriptClientException e, Map<String, Object> model) {
		// do nothing by default
	}
	
	private static String getMessage(Map<String, Object> params) {
		if (params.get(InternalParameterConstants.KEY_MESSAGE) != null) {
			return String.valueOf(params.get(InternalParameterConstants.KEY_MESSAGE));
		} else {
			return null;
		}
	}

}
