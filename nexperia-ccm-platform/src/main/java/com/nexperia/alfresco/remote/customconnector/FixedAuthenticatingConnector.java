package com.nexperia.alfresco.remote.customconnector;

import org.springframework.extensions.webscripts.connector.*;

import java.io.IOException;
import java.io.InputStream;

/**
 * Fixes core Alfresco authentication connector.
 * Just overwriting one method.
 *
 * @author Eduard Bakaev
 */
public class FixedAuthenticatingConnector extends AuthenticatingConnector {

    public FixedAuthenticatingConnector(Connector connector, Authenticator authenticator) {
        super(connector, authenticator);
    }

    public Response call(String uri, ConnectorContext context, InputStream in) {
             Response response = null;
        boolean handshake = false;

        if (isAuthenticated())
        {
	        //In case it's first call we got the 401 http error and IS won't be available for the second call
	        //So we try to mark it if it supports
	        if (in != null && in.markSupported()) {
		        in.mark(Integer.MAX_VALUE);
	        }

            // try to call into the connector to see if we can successfully do this
            response = this.connector.call(uri, context, in);

            // if there was an authentication challenge, handle here
            if (response.getStatus().getCode() == ResponseStatus.STATUS_UNAUTHORIZED)
            {
                handshake = true;
            }
        }
        else
        {
            handshake = true;
        }

        if (handshake)
        {
            handshake(); // ignore result

            // now that we've authenticated, try again
            if (in != null && in.markSupported())
            {
                try
                {
                    in.reset();
                }
                catch (IOException ioErr)
                {
                    // if we cannot reset the stream - there's nothing else we can do
                }
            }
            response = this.connector.call(uri, context, in);
       }

        return response;
    }
}