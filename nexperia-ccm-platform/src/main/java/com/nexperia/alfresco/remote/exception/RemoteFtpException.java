package com.nexperia.alfresco.remote.exception;



/**
 * Exception thrown when remote Alfresco could not run
 * FTP against remote FTP server or FTP webservice.
 * 
 * @author byaminov
 *
 */
public class RemoteFtpException extends WebScriptClientException {

	private static final long serialVersionUID = -6552361019505578603L;

	public RemoteFtpException(String message) {
		super(message);
	}

}
