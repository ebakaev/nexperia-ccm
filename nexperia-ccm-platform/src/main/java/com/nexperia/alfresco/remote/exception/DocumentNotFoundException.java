package com.nexperia.alfresco.remote.exception;

/**
 * User: Eugene Yaroslavtsev
 * Date: 16.02.2015
 */
public class DocumentNotFoundException extends WebScriptClientException {

    private static final long serialVersionUID = -6246322198339166347L;

    public DocumentNotFoundException(String message) {
        super(message);
    }
}
