package com.nexperia.alfresco.xmldb;


import com.google.common.base.Preconditions;
import com.nxp.spider2.xmldb.IAlfrescoXmlDbWrapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

/**
 * Class which provides XDB upload service implementation
 *
 * @author ebakaev
 */
public class XDBUploadServiceImpl implements XDBUploadService {

    private static final Log logger = LogFactory.getLog(XDBUploadServiceImpl.class);

    private SimpleXmlDbWrapper simpleXmlDbWrapper;

    /**
     * {@inheritDoc}
     */
    public void uploadFileToXdb(File file) {
        Preconditions.checkArgument(file != null && file.exists() && file.canRead(),
                String.format("File '%s' doesn't exist or cannot be read therefore cannot be uploaded to XDB", file));

        long start = System.currentTimeMillis();
        if (logger.isDebugEnabled()) {
            logger.debug("=== Start uploading file '" + file + "' to XDB...");
        }

        simpleXmlDbWrapper.uploadDitaZip(file);

        if (logger.isDebugEnabled()) {
            logger.debug("=== File '" + file + "' has been uploaded to XDB in "
                    + (System.currentTimeMillis() - start) + " ms");
        }
    }

    public void setXmlDb(IAlfrescoXmlDbWrapper xmlDb) {
        this.simpleXmlDbWrapper = new SynchronizedXmlDbWrapper(xmlDb);
    }

}
