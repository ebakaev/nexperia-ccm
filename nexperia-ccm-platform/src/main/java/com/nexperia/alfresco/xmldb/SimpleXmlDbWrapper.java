package com.nexperia.alfresco.xmldb;

import com.nxp.spider2.xmldb.data.external.ContentTypeId;
import com.nxp.spider2.xmldb.data.external.StateTypeId;
import org.w3c.dom.Document;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Simplified XML DB client wrapper
 *
 * @author igoonich
 * @since 13.09.2013
 */
public interface SimpleXmlDbWrapper {

    /**
     * Removes specified xml document from the collection specified by content and state types.
     * If there is no such file in the collection, the operation is ignored.
     *
     * <b>Note: name must be provided with ".xml" extension</b>
     *
     * @param documentName name of xml document to remove with extension
     * @param cTypeId      document content type - may be null
     * @param sTypeId      document state type - may be null
     */
    void removeDocument(String documentName, ContentTypeId cTypeId, StateTypeId sTypeId);

    void uploadXml(File xmlFile, ContentTypeId cTypeId, StateTypeId sTypeId);

    void uploadXml(String xmlDocumentName, Document xmlDocument, ContentTypeId cTypeId, StateTypeId sTypeId);

    void uploadXml(String xmlDocumentName, String xmlDocumentString, ContentTypeId cTypeId, StateTypeId sTypeId);

    Map<String, Exception> uploadZip(InputStream zipFileInputStream, ContentTypeId cTypeId, StateTypeId sTypeId);

    Map<String, Exception> uploadZip(File zipFile, ContentTypeId cTypeId, StateTypeId sTypeId);

    void uploadDitaZip(File zipFile);

    Resource findXmlByFileName(String fileName, ContentTypeId cTypeId, StateTypeId sTypeId);

    ResourceIterator findXmlByXPath(String xPathExpression, boolean showData, ContentTypeId cTypeId, StateTypeId sTypeId);

    List<String> findListByXQuery(InputStream xQueryStream, Map<String, Object> variables,
                                  ContentTypeId cTypeId, StateTypeId sTypeId, String fileName, long limit);

    ResourceIterator runXQuery(InputStream xQueryStream, Map<String, Object> variables,
                               ContentTypeId cTypeId, StateTypeId sTypeId, String fileName);

}
