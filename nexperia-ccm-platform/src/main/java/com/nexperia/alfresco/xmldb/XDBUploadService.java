package com.nexperia.alfresco.xmldb;

import java.io.File;

/**
 * Interface which describes methods for XDB upload
 *
 * @author ebakaev
 */
public interface XDBUploadService {

    /**
     * Uploads specified file to XDB
     *
     * @param file file to upload
     */
    void uploadFileToXdb(File file);

}
