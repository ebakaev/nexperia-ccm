package com.nexperia.alfresco.xmldb;

import com.nxp.spider2.xmldb.IAlfrescoXmlDbWrapper;
import com.nxp.spider2.xmldb.data.external.ContentTypeId;
import com.nxp.spider2.xmldb.data.external.StateTypeId;
import org.alfresco.error.AlfrescoRuntimeException;
import org.w3c.dom.Document;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.XMLDBException;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p> Alfresco thread safe simplified XML DB client wrapper. All methods are {@code synchronized}
 * because XML DB doesn't work properly while being accessed concurrently. As soon as XML DB
 * synchronization issues are fixed these modifiers can be removed.
 *
 * <p> For convenience this wrapper converts all the XDB exceptions to {@link RuntimeException}
 *
 * <p> Implements the Gateway pattern from EAA
 *
 * @author ebakaev
 * @see IAlfrescoXmlDbWrapper
 * @see AlfrescoXdbUtils#convertXdbException(Exception)
 * @since 12.04.2022
 */
public class SynchronizedXmlDbWrapper implements SimpleXmlDbWrapper {

    private final IAlfrescoXmlDbWrapper delegate;

    public SynchronizedXmlDbWrapper(IAlfrescoXmlDbWrapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public synchronized void removeDocument(String documentName, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            delegate.removeDocument(documentName, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public synchronized void uploadXml(File xmlFile, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            delegate.uploadXml(xmlFile, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public synchronized void uploadXml(String xmlDocumentName, Document xmlDocument, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            delegate.uploadXml(xmlDocumentName, xmlDocument, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public synchronized void uploadXml(String xmlDocumentName, String xmlDocumentString, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            delegate.uploadXml(xmlDocumentName, xmlDocumentString, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public synchronized Map<String, Exception> uploadZip(InputStream zipFileInputStream, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            return delegate.uploadZip(zipFileInputStream, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public synchronized Map<String, Exception> uploadZip(File zipFile, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            return delegate.uploadZip(zipFile, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public synchronized void uploadDitaZip(File zipFile) {
        try {
            delegate.setupConnection();
            delegate.uploadDitaZip(zipFile);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public Resource findXmlByFileName(String fileName, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            return delegate.findXmlByFileName(fileName, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public ResourceIterator findXmlByXPath(String xPathExpression, boolean showData, ContentTypeId cTypeId, StateTypeId sTypeId) {
        try {
            delegate.setupConnection();
            return delegate.findXmlByXPath(xPathExpression, showData, cTypeId, sTypeId);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

    @Override
    public List<String> findListByXQuery(InputStream xQueryStream, Map<String, Object> variables,
                                         ContentTypeId cTypeId, StateTypeId sTypeId, String fileName, long limit) {
        List<String> results = new ArrayList<String>();
        ResourceIterator iterator = runXQuery(xQueryStream, variables, cTypeId, sTypeId, fileName);
        try {
            for (int processed = 0; processed < limit && iterator.hasMoreResources(); processed++) {
                Resource resource = iterator.nextResource();
                Object content = resource.getContent();
                if (content != null) {
                    results.add(content.toString());
                }
            }
            return results;
        } catch (XMLDBException e) {
            throw new AlfrescoRuntimeException("Failed to iterate over xQuery result", e);
        }
    }

    @Override
    public ResourceIterator runXQuery(InputStream xQueryStream, Map<String, Object> variables,
                                      ContentTypeId cTypeId, StateTypeId sTypeId, String fileName) {
        try {
            delegate.setupConnection();
            return delegate.runXQuery(xQueryStream, variables, cTypeId, sTypeId, fileName);
        } catch (Exception e) {
            throw AlfrescoXdbUtils.convertXdbException(e);
        } finally {
            delegate.releaseConnectionQuietly();
        }
    }

}
