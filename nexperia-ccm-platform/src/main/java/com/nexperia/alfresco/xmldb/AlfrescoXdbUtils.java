package com.nexperia.alfresco.xmldb;

import com.nxp.spider2.xmldb.validation.ValidationException;
import org.alfresco.error.AlfrescoRuntimeException;
import org.xmldb.api.base.XMLDBException;

import java.io.IOException;

/**
 * @author igoonich
 * @since 13.09.2013
 */
public final class AlfrescoXdbUtils {

    /**
     * Converts XML DB exception
     * @param e     xdb exception
     */
    public static RuntimeException convertXdbException(Exception e) {
        if (e instanceof XMLDBException) {
            return new AlfrescoRuntimeException("XDB related problem", e);
        } else if (e instanceof ValidationException) {
            return new AlfrescoRuntimeException("XDB validation issue", e);
        } else if (e instanceof IOException) {
            return new AlfrescoRuntimeException("I/O problem", e);
        } else {
            return new AlfrescoRuntimeException("Unknown XDB issue", e);
        }
    }

}
