package com.nexperia.alfresco.service;

import com.nexperia.alfresco.util.CCMConstants;
import com.nexperia.alfresco.util.Util;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileFolderServiceType;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

import static com.nexperia.alfresco.util.CCMConstants.*;

/**
 * Makes request to Dita-ot Web Client docker container to convert dita to xhtml.
 * <p>
 * Container gets zipped ditamap and topics and returns zip with sources and xhtml renditions.
 */
public class DitaService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String[] validPublicationTypes = {"Value proposition", "Data sheet", "Application note", "Packing information"};

    ServiceRegistry serviceRegistry;
    Util util;
    String ditaOtWebClientUrl;

//    public File getHtmlRenditionsForDitaMapAndTopics(String nodeRef) {
//        File sourceZip = getDitaSourcesZip(nodeRef);
//        if (sourceZip == null) {
//            throw new AlfrescoRuntimeException("Dita sources are not provided!");
//        }
//        File resFileWithHtmlRenditions = TempFileProvider.createTempFile("html_" + UUID.randomUUID().toString(), "_result_.zip");
////        File ditaSourcesZip = new File("/usr/local/tomcat/temp/example.zip");
//
//        try (CloseableHttpClient client = HttpClients.createDefault()) {
//            HttpPost post = new HttpPost(ditaOtWebClientUrl);
//            HttpEntity entity = MultipartEntityBuilder.create().addPart("file", new FileBody(sourceZip)).build();
//            post.setEntity(entity);
//
//            try (CloseableHttpResponse response = client.execute(post)) {
//                logger.debug("Response: {}", response.getCode());
//                response.getEntity().writeTo(new FileOutputStream(resFileWithHtmlRenditions));
//                logger.debug("The result response was written to {}", resFileWithHtmlRenditions.getAbsolutePath());
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return resFileWithHtmlRenditions;
//    }

    private File getDitaSourcesZip(String nodeRef) {
        if (StringUtils.isEmpty(nodeRef)) {
            throw new AlfrescoRuntimeException("nodeRef of a Ditamap has to be specified for release!");
        }
        NodeRef ditaMap = new NodeRef(nodeRef);
        NodeRef parentFolder = serviceRegistry.getNodeService().getPrimaryParent(new NodeRef(nodeRef)).getParentRef();
        logger.debug("Parent folder got for packing into Zip '{}' [{}] for '{}'", util.getName(parentFolder), parentFolder, util.getName(ditaMap));
        File zipFile = TempFileProvider.createTempFile("dita_sources_" + UUID.randomUUID() + "_", ".zip");
        ZipOutputStream zipStream = null;
        try {
            zipStream = new ZipOutputStream(new FileOutputStream(zipFile));
            zipStream.setEncoding("UTF-8");
            List<FileInfo> children = serviceRegistry.getFileFolderService().list(parentFolder);
            for (FileInfo fileInfo : children) {
                addDitaSourcesToZip(zipStream, fileInfo.getNodeRef(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (zipStream != null) {
                try {
                    zipStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return zipFile;
    }

    private void appendToZip(ZipOutputStream zipStream, NodeRef nodeRef, String path) throws IOException {
        String name = util.getName(nodeRef);
        logger.debug("Processing file ==================================> " + path + name);
        final FileFolderServiceType type = serviceRegistry.getFileFolderService().getType(
                serviceRegistry.getNodeService().getType(nodeRef));
        if (FileFolderServiceType.FILE.equals(type)) {
            if (StringUtils.endsWith(name, ".dita")
                    || StringUtils.endsWith(name, ".ditamap")
                    || StringUtils.endsWith(name, ".ditaval")) {
                if (name.contains(".ditaval")) {
                    zipStream.putNextEntry(new ZipEntry(name));
                    logger.debug(name + " added to zip");
                } else {
                    zipStream.putNextEntry(new ZipEntry(path + name));
                    logger.debug(path + name + " added to zip");
                }
            } else {
                zipStream.putNextEntry(new ZipEntry("images/" + name));
                logger.debug("images/" + name + " added to zip");
            }
            ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
            if (name.contains(".dita") || name.contains(".ditamap")) {
                IOUtils.copy(cr.getContentInputStream(), zipStream);
            }
        } else if (FileFolderServiceType.FOLDER.equals(type)) {
            List<FileInfo> children = serviceRegistry.getFileFolderService().list(nodeRef);
            for (FileInfo child : children) {
                appendToZip(zipStream, child.getNodeRef(), "topics/");
            }
        }
    }

    public void addDitaSourcesToZip(ZipOutputStream zipStream, NodeRef nodeRef, String parentDirectoryName) throws Exception {
        if (nodeRef == null || !serviceRegistry.getNodeService().exists(nodeRef)) {
            return;
        }
        FileFolderServiceType type = serviceRegistry.getFileFolderService().getType(serviceRegistry.getNodeService().getType(nodeRef));

        String zipEntryName = util.getName(nodeRef);
        if (parentDirectoryName != null && !parentDirectoryName.isEmpty()) {
            zipEntryName = parentDirectoryName + "/" + zipEntryName;
        }

        if (FileFolderServiceType.FOLDER.equals(type)) {
            System.out.println("+" + zipEntryName);
            List<FileInfo> children = serviceRegistry.getFileFolderService().list(nodeRef);
            for (FileInfo child : children) {
                addDitaSourcesToZip(zipStream, child.getNodeRef(), zipEntryName);
            }
        } else {
            System.out.println("   " + zipEntryName);
            zipStream.putNextEntry(new ZipEntry(zipEntryName));
            ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
            IOUtils.copy(cr.getContentInputStream(), zipStream);
            zipStream.closeEntry();
        }
    }

    /**
     * Checks 'id' value of a map.
     * <map id="" ..."/>
     *
     * @return id value of a map.
     */
    public void updateDataSheetIdParameter(NodeRef nodeRef, String prefix) {
        ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
        BufferedReader reader = null;
        StringBuilder contentToWrite = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(cr.getContentInputStream(), "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(MAP_TAG) && line.contains(REV_ATTR) && line.contains(ID_ATTR)) {
                    if (StringUtils.isNotEmpty(line)) {
                        String mapId = StringUtils.substringBetween(line, "id=\"", "\"");
                        logger.debug("Map id is: " + mapId);
                        if (Character.isDigit(mapId.charAt(0))) {
                            contentToWrite.append(line.replace(mapId, prefix + mapId));
                        } else {
                            contentToWrite.append(line);
                        }
                    } else {
                        contentToWrite.append(line);
                    }
                } else {
                    contentToWrite.append(line);
                }
                contentToWrite.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //logger.debug(contentToWrite.toString());
        ContentWriter contentWriter = serviceRegistry.getContentService().getWriter(nodeRef, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent(contentToWrite.toString());
    }

    public boolean isMapIdValid(NodeRef nodeRef, String prefix) {
        ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(cr.getContentInputStream(), "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(MAP_TAG) && line.contains(REV_ATTR) && line.contains(ID_ATTR)) {
                    if (StringUtils.isNotEmpty(line)) {
                        String mapId = StringUtils.substringBetween(line, "id=\"", "\"");
                        if (!Character.isDigit(mapId.charAt(0))) {
                            logger.debug("Map id is valid");
                        } else {
                            logger.debug("Map id isn't valid, updating");
                        }
                        return !Character.isDigit(mapId.charAt(0));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }

    public void updateTopicId(NodeRef nodeRef) {
        ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
        BufferedReader reader = null;
        StringBuilder contentToWrite = new StringBuilder();
        String topicName = util.getName(nodeRef);
        try {
            reader = new BufferedReader(new InputStreamReader(cr.getContentInputStream(), "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(TOPIC_TAG)) {
                    if (StringUtils.isNotEmpty(line)) {
                        String topicId = StringUtils.substringBetween(line, "id=\"", "\"");
                        String correctId = FilenameUtils.getBaseName(util.getName(nodeRef));
                        logger.debug("Topic id is: " + topicId);
                        if (topicName.equals(topicId)) {
                            contentToWrite.append(line);
                        } else if (StringUtils.isNotEmpty(topicId)) {
                            contentToWrite.append(line.replace(topicId, correctId));
                        } else {
                            contentToWrite.append(line.replace("<topic", String.format("<topic id=\"%s\" ", correctId)));
                        }
                    } else {
                        contentToWrite.append(line);
                    }
                } else {
                    contentToWrite.append(line);
                }
                contentToWrite.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        ContentWriter contentWriter = serviceRegistry.getContentService().getWriter(nodeRef, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent(contentToWrite.toString());
    }

    public boolean isTopicIdValid(NodeRef nodeRef) {
        ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(cr.getContentInputStream(), "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(TOPIC_TAG)) {
                    if (StringUtils.isNotEmpty(line)) {
                        String topicId = StringUtils.substringBetween(line, "id=\"", "\"");
                        String correctId = FilenameUtils.getBaseName(util.getName(nodeRef));
                        if (topicId.equals(correctId)) {
                            logger.debug("Topic id is valid");
                        } else {
                            logger.debug("Topic id isn't valid, updating");
                        }
                        return topicId.equals(correctId);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }

    /**
     * Extracts document type value from ditamap content.
     *
     * @param dita
     * @return map of values which define document type and linked VP if document type is Data sheet
     */
    public Map<String, String> parseDocumentTypeOfTheDitaMap(NodeRef dita) {
        String publicationTypeValue = getDocumentTypeByTagName(dita, PUBLICATION_TYPE_TAG);
        Map<String, String> dTypes = new HashMap<>(1);

        //If ditamap is Subject scheme we pass through other checks...
        if (StringUtils.isEmpty(publicationTypeValue) && containsSubjectSchemeTagName(dita)) {
            dTypes.put(PUBLICATION_TYPE_TAG, DOCUMENT_TYPE_SUBJECT_SCHEME);
            return dTypes;
        }

        String docIdentifier = getDocumentTypeByTagName(dita, DOC_VP_TAG);
        logger.debug("Document type from ditamap for 'publication-type' is: " + publicationTypeValue);
        if (StringUtils.isNotEmpty(publicationTypeValue)
                && !Arrays.asList(validPublicationTypes).contains(publicationTypeValue)) {
            throw new AlfrescoRuntimeException("Please check <othermeta content=\"" + publicationTypeValue
                    + "\" name=\"publication-type\"/> content value. It has invalid content value. Correct values are: " + Arrays.toString(validPublicationTypes));
        }
        if (StringUtils.isEmpty(publicationTypeValue)) {
            throw new AlfrescoRuntimeException("Please check <othermeta content=\"\" name=\"publication-type\"/> value. It is mandatory. Correct values are: " + Arrays.toString(validPublicationTypes));
        }
        dTypes.put(PUBLICATION_TYPE_TAG, publicationTypeValue);
        if (StringUtils.isNotEmpty(docIdentifier)) {
            dTypes.put(DOC_VP_TAG, docIdentifier);
        }

        return dTypes;
    }

    /**
     * Returns 'content' value of othermeta by its name value.
     * <othermeta name="type" content="value proposition"/>
     * or
     * <othermeta name="publication-type" content="Data sheet"/>
     *
     * @param tag name value of othermata tag
     * @return content value of othermeta tag
     */
    public String getDocumentTypeByTagName(NodeRef nodeRef, String tag) {
        ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(cr.getContentInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(tag)) {
                    if (StringUtils.isNotEmpty(line)) {
                        return StringUtils.substringBetween(line, "content=\"", "\"");
                    } else {
                        return null;
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * Returns 'true' if it's not a map but <subjectScheme/>
     */
    public Boolean containsSubjectSchemeTagName(NodeRef nodeRef) {
        ContentReader cr = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(cr.getContentInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(SUBJECT_SCHEME_TAG)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean isDitaMapFile(String name) {
        return name.contains(".ditamap");
    }

    public boolean isDitaTopicFile(String name) {
        return name.contains(".dita");
    }

    public void setDitaOtWebClientUrl(String ditaOtWebClientUrl) {
        this.ditaOtWebClientUrl = ditaOtWebClientUrl;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public boolean isDataSheetDocument(String nodeRef) {
        NodeService nodeService = serviceRegistry.getNodeService();
        if (util.isDataSheet(nodeRef)) {
            return true;
        } else {
            Map<String, String> docTypes = parseDocumentTypeOfTheDitaMap(new NodeRef(nodeRef));
            if (CCMConstants.DOCUMENT_TYPE_DATA_SHEET.equals(docTypes.get(PUBLICATION_TYPE_TAG)) && !nodeService.hasAspect(new NodeRef(nodeRef), CCMConstants.ASPECT_DATA_SHEET)) {
                nodeService.addAspect(new NodeRef(nodeRef), CCMConstants.ASPECT_DATA_SHEET, null);
                return true;
            }
        }
        return false;
    }

    public String getLinkedVPNodeRef(String nodeRef) {
        FileFolderService ffService = serviceRegistry.getFileFolderService();
        NodeService nodeService = serviceRegistry.getNodeService();
        NodeRef parent = nodeService.getPrimaryParent(new NodeRef(nodeRef)).getParentRef();
        List<FileInfo> files = ffService.listFiles(parent);
        String fileToSearch = getLinkedVP(nodeRef);
        logger.debug("Found linked VP value in content: " + fileToSearch);
        for (FileInfo fileInfo : files) {
            if (fileInfo.getName().equals(fileToSearch)) {
                logger.debug("Real file found in the same space with NodeRef: {}", fileInfo.getNodeRef());
                return fileInfo.getNodeRef().toString();
            }
        }
        return null;
    }

    /**
     * Searches for the linked VP of a Data sheet.
     * It looks for <mapref href="vp_EEE.ditamap"/> and returns href value.
     *
     * @return linked VP of a Data sheet
     */
    public String getLinkedVP(String nodeRef) {
        ContentReader cr = serviceRegistry.getContentService().getReader(new NodeRef(nodeRef), ContentModel.PROP_CONTENT);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(cr.getContentInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(MAP_REF_TAG) && line.contains(MAP_REF_FORMAT_TAG)) {
                    if (StringUtils.isNotEmpty(line)) {
                        String value = StringUtils.substringBetween(line, MAP_REF_VALUE_START, MAP_REF_VALUE_END);
                        if (value.contains("vp_")) {
                            return value;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
