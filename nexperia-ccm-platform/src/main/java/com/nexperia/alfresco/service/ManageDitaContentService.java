package com.nexperia.alfresco.service;

import com.nexperia.alfresco.util.Util;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static com.nexperia.alfresco.util.CCMConstants.*;

public class ManageDitaContentService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private NodeService nodeService;
    private DitaService ditaService;
    private Util util;

    public void checkContentAndMarkDitaMap(NodeRef dita) {
        //checking document content to get it's document type.
        String fileName = util.getName(dita);
        if (ditaService.isDitaMapFile(fileName) && !nodeService.hasAspect(dita, ContentModel.ASPECT_WORKING_COPY)) {
            logger.debug("Checkin/Updating/Marking {} ditaMap", fileName);
            Map<String, String> dTypes = ditaService.parseDocumentTypeOfTheDitaMap(dita);
            QName aspectToKeep = null;
            if (DOCUMENT_TYPE_VALUE_PROPOSITION.equals(dTypes.get(PUBLICATION_TYPE_TAG)) && !nodeService.hasAspect(dita, ASPECT_VALUE_PROPOSITION)) {
                logger.debug("adding ASPECT_VALUE_PROPOSITION to {}", fileName);
                nodeService.addAspect(dita, ASPECT_VALUE_PROPOSITION, null);
                aspectToKeep = ASPECT_VALUE_PROPOSITION;
            }
            if (DOCUMENT_TYPE_DATA_SHEET.equals(dTypes.get(PUBLICATION_TYPE_TAG)) && !nodeService.hasAspect(dita, ASPECT_DATA_SHEET)) {
                logger.debug("adding ASPECT_DATA_SHEET to {}", fileName);
                nodeService.addAspect(dita, ASPECT_DATA_SHEET, null);
                aspectToKeep = ASPECT_DATA_SHEET;
            }
            if (DOCUMENT_TYPE_APPLICATION_NOTE.equals(dTypes.get(PUBLICATION_TYPE_TAG)) && !nodeService.hasAspect(dita, ASPECT_APPLICATION_NOTE)) {
                logger.debug("adding ASPECT_APPLICATION_NOTE to {}", fileName);
                nodeService.addAspect(dita, ASPECT_APPLICATION_NOTE, null);
                aspectToKeep = ASPECT_APPLICATION_NOTE;
            }
            if (DOCUMENT_TYPE_PACKING_INFORMATION.equals(dTypes.get(PUBLICATION_TYPE_TAG)) && !nodeService.hasAspect(dita, ASPECT_PACKING_INFORMATION)) {
                logger.debug("adding ASPECT_PACKING_INFORMATION to {}", fileName);
                nodeService.addAspect(dita, ASPECT_PACKING_INFORMATION, null);
                aspectToKeep = ASPECT_PACKING_INFORMATION;
            }
            if (DOCUMENT_TYPE_SUBJECT_SCHEME.equals(dTypes.get(PUBLICATION_TYPE_TAG)) && !nodeService.hasAspect(dita, ASPECT_SUBJECT_SCHEME)) {
                logger.debug("adding ASPECT_SUBJECT_SCHEME to {}", fileName);
                nodeService.addAspect(dita, ASPECT_SUBJECT_SCHEME, null);
                aspectToKeep = ASPECT_SUBJECT_SCHEME;
            }
            if (aspectToKeep != null) {
                cleanMarkerAspects(dita, aspectToKeep, fileName);
            }
        }
    }

    private void cleanMarkerAspects(NodeRef ditaMap, QName aspectToKeep, String fileName) {
        if (nodeService.hasAspect(ditaMap, ASPECT_PACKING_INFORMATION) && ASPECT_PACKING_INFORMATION != aspectToKeep) {
            logger.debug("removing ASPECT_PACKING_INFORMATION from {}", fileName);
            nodeService.removeAspect(ditaMap, ASPECT_PACKING_INFORMATION);
        }
        if (nodeService.hasAspect(ditaMap, ASPECT_APPLICATION_NOTE) && ASPECT_APPLICATION_NOTE != aspectToKeep) {
            logger.debug("removing ASPECT_APPLICATION_NOTE from {}", fileName);
            nodeService.removeAspect(ditaMap, ASPECT_APPLICATION_NOTE);
        }
        if (nodeService.hasAspect(ditaMap, ASPECT_DATA_SHEET) && ASPECT_DATA_SHEET != aspectToKeep) {
            logger.debug("removing ASPECT_DATA_SHEET from {}", fileName);
            nodeService.removeAspect(ditaMap, ASPECT_DATA_SHEET);
        }
        if (nodeService.hasAspect(ditaMap, ASPECT_VALUE_PROPOSITION) && ASPECT_VALUE_PROPOSITION != aspectToKeep) {
            logger.debug("removing ASPECT_VALUE_PROPOSITION from {}", fileName);
            nodeService.removeAspect(ditaMap, ASPECT_VALUE_PROPOSITION);
        }
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setDitaService(DitaService ditaService) {
        this.ditaService = ditaService;
    }

    public void setUtil(Util util) {
        this.util = util;
    }
}
