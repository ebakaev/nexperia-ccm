package com.nexperia.alfresco.service;

import org.alfresco.util.TempFileProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TidyService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private boolean isServiceEnabled = false;
    private String tidyShScriptLocation;

    public File clean(File source) throws IOException, InterruptedException {

        if (isServiceEnabled) {
            File sourceFile = TempFileProvider.createTempFile("tidy-source_" + (new Date()).getTime(), ".in");
            logger.debug("Copying source file [{}] to temp [{}]", source.getAbsolutePath(), sourceFile.getAbsolutePath());
            FileCopyUtils.copy(source, sourceFile);

            Runtime rt = Runtime.getRuntime();
            List<String> cmd = new ArrayList<String>();

            String sourceFilePath = sourceFile.getAbsolutePath().replace("\\", "/");
            String resFilePath = sourceFile.getAbsolutePath().replace("\\", "/").replace(".in", ".out").replace("source_", "result_");
            cmd.add("sh");
            cmd.add(tidyShScriptLocation);
            cmd.add(sourceFilePath);
            cmd.add(resFilePath);
            String[] cmdArgs = cmd.toArray(new String[0]);
            Process proc = rt.exec(cmdArgs);
            if (logger.isDebugEnabled()) {
                logger.debug("Command executed : " + cmd.toString().replaceAll(",", " "));
            }
            StreamGobbler errorGobbler = new
                    StreamGobbler(proc.getErrorStream(), "ERROR");

            // any output?
            StreamGobbler outputGobbler = new
                    StreamGobbler(proc.getInputStream(), "OUTPUT");

            // kick them off
            errorGobbler.start();
            outputGobbler.start();

            // any error???
            int exitVal = proc.waitFor();
            if (logger.isDebugEnabled()) {
                logger.debug("Process exitValue: " + exitVal);
            }
            File convertedFile = new File(resFilePath);
            if (!convertedFile.exists()) {
                logger.error("Tidy cleaned file isn't found, please check path to tidy, file: " + resFilePath);
                throw new FileNotFoundException(resFilePath + " was not found");
            }

            // success
            if (logger.isDebugEnabled()) {
                logger.debug("Tidy executed successfully.");
            }

            return convertedFile;
        }

        return source;
    }

    private class StreamGobbler extends Thread {
        InputStream is;
        String type;
        OutputStream os;

        StreamGobbler(InputStream is, String type) {
            this(is, type, System.out);
        }

        StreamGobbler(InputStream is, String type, OutputStream redirect) {
            this.is = is;
            this.type = type;
            this.os = redirect;
        }

        public void run() {
            try {
                PrintWriter pw = null;
                if (os != null)
                    pw = new PrintWriter(os);

                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    if (pw != null)
                        pw.println(line);
                }
                if (pw != null)
                    pw.flush();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void setServiceEnabled(boolean serviceEnabled) {
        isServiceEnabled = serviceEnabled;
    }

    public void setTidyShScriptLocation(String tidyShScriptLocation) {
        this.tidyShScriptLocation = tidyShScriptLocation;
    }
}
