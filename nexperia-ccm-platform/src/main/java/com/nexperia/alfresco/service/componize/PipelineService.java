package com.nexperia.alfresco.service.componize;

import com.componize.alfresco.repo.xml.pipeline.AlfrescoPipelineDefinition;
import com.componize.alfresco.repo.xml.pipeline.AlfrescoPipelineRegistry;
import com.componize.alfresco.repo.xml.pipeline.AlfrescoPipelineService;
import com.componize.alfresco.repo.xml.pipeline.AlfrescoPipelineSource;
import com.componize.xml.dita.DitaPipelineDefinitions;
import com.nexperia.alfresco.util.Util;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PipelineService {

    private static final Logger logger = LoggerFactory.getLogger(PipelineService.class);

    ServiceRegistry serviceRegistry;
    Util util;
    AlfrescoPipelineRegistry pipelineRegistry;
    AlfrescoPipelineService pipelineService;
    String outputSpace;
    String sourceSpace;

    /**
     * Get pipeline source of the ZIP pipeline, which creates a ZIP
     * file with a DITA map and all referenced topics and images.
     * Default source base is language space of current document, and
     * default result base is "Output" folder in the language space.
     *
     * @param nodeRef node to use for the pipeline source
     * @return configured pipeline source.
     */
    public AlfrescoPipelineSource getZipPipelineSource(NodeRef nodeRef) {

        AlfrescoPipelineDefinition definition = pipelineRegistry.getPipelineDefinition(DitaPipelineDefinitions.MAP2ZIP);
        AlfrescoPipelineSource pipelineSource = pipelineService.newAlfrescoPipelineSource(nodeRef, definition);

        // Set output space
        if (pipelineSource.getResultBase() == null) {
            pipelineSource.setResultBase(util.getNodeByPath(outputSpace));
        }
        // Set root space relative for files in the ZIP generated
        pipelineSource.setSourceBase(util.getNodeByPath(sourceSpace));

        return pipelineSource;
    }

    /**
     * Runs componize pipeline
     *
     * @param zipPipelineSource - pipeline to run
     */
    public void runPipeline(AlfrescoPipelineSource zipPipelineSource) {
        pipelineService.runPipeline(zipPipelineSource);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setPipelineRegistry(AlfrescoPipelineRegistry pipelineRegistry) {
        this.pipelineRegistry = pipelineRegistry;
    }

    public void setPipelineService(AlfrescoPipelineService pipelineService) {
        this.pipelineService = pipelineService;
    }

    public void setOutputSpace(String outputSpace) {
        this.outputSpace = outputSpace;
    }

    public void setSourceSpace(String sourceSpace) {
        this.sourceSpace = sourceSpace;
    }

    public void setUtil(Util util) {
        this.util = util;
    }
}