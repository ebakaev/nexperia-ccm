package com.nexperia.alfresco.service;

import com.componize.alfresco.repo.xml.dita.DitaModel;
import com.componize.alfresco.repo.xml.pipeline.AlfrescoPipelineSource;
import com.nexperia.S3Client;
import com.nexperia.alfresco.remote.client.PublicationClient;
import com.nexperia.alfresco.remote.client.PublicationConstants;
import com.nexperia.alfresco.service.componize.PipelineService;
import com.nexperia.alfresco.service.transform.ditaot.DitaOtService;
import com.nexperia.alfresco.service.transform.oxygen.OxygenService;
import com.nexperia.alfresco.util.CCMConstants;
import com.nexperia.alfresco.util.Util;
import com.nexperia.alfresco.xmldb.XDBUploadService;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static com.nexperia.alfresco.util.CCMConstants.*;

public class ReleaseService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    PipelineService pipelineService;
    ServiceRegistry serviceRegistry;
    PublicationClient publicationClient;
    S3Client s3Client;
    OxygenService oxygenService;
    DitaOtService ditaOtService;
    Util util;
    TransformService transformService;
    Boolean cleanTempSources = false;
    XDBUploadService xdbUploadService;
    TidyService tidyService;
    private Map<String, Map<String, String>> componize2spider;
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public String releaseMapUsingDitaOt(String nodeRef, boolean isDatasheet) throws IOException {
        String publicationId;
        //Run Componize pipeline to collect all ref topics from diff places to one folder for following manipulation
        NodeRef zipSource = collectMapAndDependencies(nodeRef);
        //Creating ZIP file to publish. ZIP contains generated HTML renditions.
        File zipToRelease = collectZipToReleaseByDitaOt(zipSource, isDatasheet);
        //Publishing the ZIP to SPIDER Library
        Map<String, String> publicationMetadata = collectMetadata(nodeRef, isDatasheet);
        publicationMetadata.put(PublicationConstants.FILE_NAME, zipToRelease.getName());
        publicationId = release(new FileInputStream(zipToRelease), publicationMetadata);
        //Sending ZIP to AWS S3
        s3Client.releaseDocument(publicationMetadata.get(PublicationConstants.FILE_NAME), new FileInputStream(zipToRelease), isDatasheet ? DOCUMENT_TYPE_DS : DOCUMENT_TYPE_VP);
        logger.debug("{} has been sent to S3 bucket", publicationMetadata.get(PublicationConstants.FILE_NAME));
        //Sending ZIP to XMLDB Sedna
        xdbUploadService.uploadFileToXdb(zipToRelease);

        //Cleanup temp files in Alfresco
        if (cleanTempSources) {
            serviceRegistry.getNodeService().deleteNode(zipSource);
            serviceRegistry.getNodeService().deleteNode(serviceRegistry.getNodeService().getPrimaryParent(zipSource).getParentRef());
        }

        return publicationId;
    }

    public String releaseMap(String nodeRef) throws IOException {
        String publicationId;
        //Run Componize pipeline to collect all ref topics from diff places to one folder for following manipulation
        NodeRef zipSource = collectMapAndDependencies(nodeRef);
        //Creating ZIP file to publish. ZIP contains generated HTML renditions.
        NodeRef zipToRelease = collectZipToRelease(zipSource);
        //Publishing the ZIP to SPIDER Library
        Map<String, String> publicationMetadata = collectMetadata(nodeRef, false);
        publicationMetadata.put(PublicationConstants.FILE_NAME, util.getName(zipToRelease));
        ContentReader contentReader = serviceRegistry.getContentService().getReader(zipToRelease, ContentModel.PROP_CONTENT);
        publicationId = release(contentReader.getContentInputStream(), publicationMetadata);

        contentReader = serviceRegistry.getContentService().getReader(zipToRelease, ContentModel.PROP_CONTENT);
        s3Client.releaseDocument(publicationMetadata.get(PublicationConstants.FILE_NAME), contentReader.getContentInputStream(), DOCUMENT_TYPE_VP);
        logger.debug("{} has been sent to S3 bucket", publicationMetadata.get(PublicationConstants.FILE_NAME));

        //Sending ZIP to XMLDB Sedna
        File xmlDbFile = TempFileProvider.createTempFile("zipToRelease_ " + publicationMetadata.get(PublicationConstants.FILE_NAME), ".zip");
        ContentReader zioContentReader = serviceRegistry.getContentService().getReader(zipToRelease, ContentModel.PROP_CONTENT);
        IOUtils.copy(zioContentReader.getContentInputStream(), new FileOutputStream(xmlDbFile));
        xdbUploadService.uploadFileToXdb(xmlDbFile);

        //Cleanup temp files in Alfresco
        if (cleanTempSources) {
            serviceRegistry.getNodeService().deleteNode(zipSource);
            serviceRegistry.getNodeService().deleteNode(serviceRegistry.getNodeService().getPrimaryParent(zipToRelease).getParentRef());
        }

        return publicationId;
    }

    private File collectZipToReleaseByDitaOt(NodeRef zipSource, boolean isDatasheet) throws IOException {
        NodeService nodeService = serviceRegistry.getNodeService();
        ContentService contentService = serviceRegistry.getContentService();
        String zipSourceName = util.getName(zipSource);

        NodeRef parentFolder = nodeService.getPrimaryParent(zipSource).getParentRef();
        logger.debug("Parent folder name is {}", util.getName(parentFolder));
        String resultFolderName = FilenameUtils.getBaseName(zipSourceName);
        NodeRef extractedZipFolder = createContent(parentFolder, resultFolderName, ContentModel.TYPE_FOLDER);
        logger.debug("Unzipped folder {} [{}]", util.getName(extractedZipFolder), extractedZipFolder);

        ContentReader zipSourceContent = contentService.getReader(zipSource, ContentModel.PROP_CONTENT);
        ZipInputStream zis = new ZipInputStream(zipSourceContent.getContentInputStream());
        ZipEntry zipEntry = zis.getNextEntry();
        File workFolder = new File(TempFileProvider.getTempDir().getAbsolutePath() + "/" + (new Date()).getTime() + "_" + resultFolderName);
        workFolder.mkdir();
        File ditaMapToHTML = null;
        String ditaMapName = null;
        while (zipEntry != null) {
            String entryName = zipEntry.getName();
            if (entryName.contains("/")) {
                entryName = entryName.substring(entryName.lastIndexOf("/") + 1);
            }
            File file;
            InputStream inputStream;
            if (entryName.contains(".ditamap")) {
                file = new File(workFolder + "/" + entryName);
                if (entryName.equals(resultFolderName + ".ditamap")) {
                    ditaMapToHTML = file;
                    ditaMapName = entryName;
                }
                inputStream = new ByteArrayInputStream(zis.readAllBytes());
                logger.debug("Created file {} [{}]", entryName, file);
            } else if (entryName.contains(".dita")) {
                File topicFolder = new File(workFolder + "/topics");
                topicFolder.mkdir();
                file = new File(topicFolder + "/" + entryName);
                inputStream = new ByteArrayInputStream(zis.readAllBytes());
                logger.debug("Created file {} [{}]", entryName, file);
            } else {
                File imagesFolder = new File(workFolder + "/images");
                imagesFolder.mkdir();
                file = new File(imagesFolder + "/" + entryName);
                inputStream = new ByteArrayInputStream(zis.readAllBytes());
                logger.debug("Created file {} [{}]", entryName, file);
            }
            IOUtils.copy(inputStream, new FileOutputStream(file));

            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();

        if (ditaMapToHTML != null) {
            if (!isDatasheet) {
                //Generate ditamap HTML
                NodeRef ditaMap = createContent(extractedZipFolder, ditaMapToHTML.getName(), ContentModel.TYPE_CONTENT);
                ContentWriter fileWriter = contentService.getWriter(ditaMap, ContentModel.PROP_CONTENT, true);
                fileWriter.guessMimetype(ditaMapToHTML.getName());
                fileWriter.putContent(new FileInputStream(ditaMapToHTML));
                InputStream htmlRendition = transformService.generateHtmlRendition(ditaMap, ditaMapToHTML.getName());
                File ditaHtml = new File(workFolder + "/" + ditaMapToHTML.getName().replace(".ditamap", ".html"));
                IOUtils.copy(htmlRendition, new FileOutputStream(ditaHtml));
            }
            String resultFolder = null;
            try {
                resultFolder = ditaOtService.transformDitaToHtml(ditaMapName, ditaMapToHTML.getAbsolutePath().replace("\\", "/"), workFolder.getAbsolutePath().replace("\\", "/"), isDatasheet);
                logger.debug("DITA-OT: folder with results '{}'", resultFolder);
                if (isDatasheet) {
                    logger.debug("Data sheet: index file location '{}'", resultFolder);
                    String indexFileAbsosuteLocation = resultFolder + resultFolder.substring(resultFolder.lastIndexOf("/temp")) + "/" + ditaMapName.replace(".ditamap", ".html");
                    logger.debug("Data sheet: index file abs path '{}'", indexFileAbsosuteLocation);
                    logger.debug("Data sheet: moving index file to '{}'", resultFolder);
                    File resultFile = new File(indexFileAbsosuteLocation);
                    resultFile.renameTo(new File(resultFolder + "/" + ditaMapName.replace(".ditamap", ".html")));
                }
            } catch (InterruptedException e) {
                logger.error("Oxygen conversion failed!", e);
                e.printStackTrace();
            }

            String resultZipPath = workFolder + "/" + resultFolderName + ".zip";
            util.zipDirectory(workFolder, resultZipPath, tidyService);
            return new File(resultZipPath);
        }

        return null;
    }

    public void releaseTopic(String nodeRef) throws FileNotFoundException {
        NodeRef topicNodeRef = new NodeRef(nodeRef);
        Map<String, String> publicationMetadata = collectMetadata(nodeRef, false);
        String nexperiaTitle = (String) serviceRegistry.getNodeService().getProperty(topicNodeRef, ContentModel.PROP_TITLE);
        publicationMetadata.put(PublicationConstants.FILE_NAME, util.getName(nodeRef));
        publicationMetadata.put(PublicationConstants.DESCRIPTIVE_TITLE, nexperiaTitle);
        publicationMetadata.put(PublicationConstants.DOCUMENT_TYPE, DOCUMENT_TYPE_TOPIC);

        Map<String, String> publicationParameters = new HashMap<String, String>();
        /*  Set Publish To Web = false as required   */
        publicationParameters.put(PublicationConstants.PUBLISHED_TO_WEB, Boolean.FALSE.toString());

        /*  Publish rendition content */
        ContentReader reader = serviceRegistry.getContentService().getReader(topicNodeRef, ContentModel.PROP_CONTENT);
        String publicationId = publicationClient.publish(reader.getContentInputStream(), publicationMetadata, publicationParameters);
        logger.debug("Topic {} was successfully published to SPIDER Library with {} publication ID", util.getName(nodeRef), publicationId);
    }

    private String release(InputStream zipFile, Map<String, String> metadata) {
        String fileName = metadata.get(PublicationConstants.FILE_NAME);
        if (logger.isDebugEnabled()) {
            logger.debug("Publishing {}...", fileName);
        }
        try {
            String publicationId = publicationClient.publish(zipFile, metadata);
            if (logger.isDebugEnabled()) {
                logger.debug("'{}' was successfully published. The Publication ID is {}", fileName, publicationId);
            }
            return publicationId;
        } catch (ContentIOException e) {
            throw new AlfrescoRuntimeException("Publication could not be executed. " + e.getMessage(), e);
        }
    }

    private Map<String, String> collectMetadata(String nodeRef, boolean isDatasheet) {
        //gets metadata from Drawing space. Its parent or parent of parent for topic (can be in diff Drawing... ) usually.
        Map<QName, Serializable> sourceMeta = serviceRegistry.getNodeService().getProperties(new NodeRef(nodeRef));
        String fileName = util.getName(nodeRef);
        String defaultTitle = FilenameUtils.getBaseName(fileName);

        Map<String, String> map = new HashMap<String, String>();
        map.put(PublicationConstants.LANGUAGE_CODE, "en");

        /* Put other metadata */
        put(map, sourceMeta, PublicationConstants.TITLE, ContentModel.PROP_TITLE, defaultTitle);
        put(map, sourceMeta, PublicationConstants.DESCRIPTION, ContentModel.PROP_DESCRIPTION);
        put(map, sourceMeta, PublicationConstants.DESCRIPTIVE_TITLE, ContentModel.PROP_DESCRIPTION);
        put(map, sourceMeta, PublicationConstants.CREATOR, ContentModel.PROP_CREATOR);
        put(map, sourceMeta, PublicationConstants.MODIFIER, ContentModel.PROP_MODIFIER);
        put(map, sourceMeta, PublicationConstants.AUTHOR, ContentModel.PROP_AUTHOR);
        put(map, sourceMeta, PublicationConstants.KEYWORDS, DitaModel.PROP_KEYWORD);

        map.put(PublicationConstants.DOCUMENT_STATUS, CCMConstants.STATUS_RELEASED);
        map.put(PublicationConstants.SECURITY_STATUS, CCMConstants.SECURITY_STATUS_COMPANY_PUBLIC);
        map.put(PublicationConstants.CREATION_DATE, dateFormatter.format((Date) sourceMeta.get(ContentModel.PROP_CREATED)));
        map.put(PublicationConstants.MODIFICATION_DATE, dateFormatter.format((Date) sourceMeta.get(ContentModel.PROP_MODIFIED)));
        map.put(PublicationConstants.RELEASED_DATE, dateFormatter.format(new Date()));
        map.put(PublicationConstants.RELEASER, serviceRegistry.getAuthenticationService().getCurrentUserName());
        map.put(PublicationConstants.APPROVER, serviceRegistry.getAuthenticationService().getCurrentUserName());
        map.put(PublicationConstants.PUBLISHED_TO_WEB, Boolean.TRUE.toString());
        if ("Data sheet".equals(map.get(PublicationConstants.DOCUMENT_TYPE))) {
            map.put(PublicationConstants.PAGE_COUNT, "0");
        }

        String description = (String) serviceRegistry.getNodeService().getProperty(new NodeRef(nodeRef), ContentModel.PROP_DESCRIPTION);
        map.put(PublicationConstants.DESCRIPTIVE_TITLE, description);
        if (isDatasheet) {
            map.put(PublicationConstants.DOCUMENT_TYPE, DOCUMENT_TYPE_DS);
            map.put(PublicationConstants.PAGE_COUNT, "1");
        } else {
            map.put(PublicationConstants.DOCUMENT_TYPE, DOCUMENT_TYPE_VP);
        }

        return map;
    }

    private void put(Map<String, String> map, Map<QName, Serializable> metadata, String key, QName property) {
        put(map, metadata, key, property, null);
    }

    @SuppressWarnings("unchecked")
    private void put(Map<String, String> map, Map<QName, Serializable> metadata, String key, QName property, String defaultValue) {
        if (metadata.get(property) != null) {
            String rawValue;
            Serializable object = metadata.get(property);
            if (object instanceof Collection) {
                if (((Collection) object).size() == 0) {
                    rawValue = "";
                } else {
                    rawValue = StringUtils.join((Collection) object, ", ");
                }
            } else {
                rawValue = String.valueOf(object);
            }
            String value = rawValue;
            String shortQName = property.toPrefixString(serviceRegistry.getNamespaceService());
            if (componize2spider.containsKey(shortQName)) {
                if (componize2spider.get(shortQName).containsKey(rawValue)) {
                    value = componize2spider.get(shortQName).get(rawValue);
                }
            }

            map.put(key, value);
        } else if (defaultValue != null) {
            map.put(key, defaultValue);
        }
    }

    private NodeRef collectZipToRelease(NodeRef zipSource) throws IOException {
        NodeService nodeService = serviceRegistry.getNodeService();
        ContentService contentService = serviceRegistry.getContentService();

        NodeRef parentFolder = nodeService.getPrimaryParent(zipSource).getParentRef();
        logger.debug("Parent folder name is {}", util.getName(parentFolder));
        String resultFolderName = FilenameUtils.getBaseName(util.getName(zipSource));
        NodeRef extractedZipFolder = createContent(parentFolder, resultFolderName, ContentModel.TYPE_FOLDER);
        logger.debug("Unzipped folder {} [{}]", util.getName(extractedZipFolder), extractedZipFolder);
        NodeRef images = createContent(extractedZipFolder, "images", ContentModel.TYPE_FOLDER);
        NodeRef topics = createContent(extractedZipFolder, "topics", ContentModel.TYPE_FOLDER);

        NodeRef resultZip = createContent(extractedZipFolder, resultFolderName + ".zip", ContentModel.TYPE_CONTENT);
        ContentWriter resultWriter = contentService.getWriter(resultZip, ContentModel.PROP_CONTENT, true);
        resultWriter.guessMimetype(resultFolderName + ".zip");
        ZipOutputStream zipOut = new ZipOutputStream(resultWriter.getContentOutputStream());

        ContentReader zipSourceContent = contentService.getReader(zipSource, ContentModel.PROP_CONTENT);
        ZipInputStream zis = new ZipInputStream(zipSourceContent.getContentInputStream());
        ZipEntry zipEntry = zis.getNextEntry();
        byte[] buffer = new byte[1024];
        while (zipEntry != null) {
            String entryName = zipEntry.getName();
            if (entryName.contains("/")) {
                entryName = entryName.substring(entryName.lastIndexOf("/") + 1);
            }
            NodeRef file;
            InputStream inputStream;
            boolean isMap = false;
            boolean isTopic = false;
            if (entryName.contains(".ditamap")) {
                file = createContent(extractedZipFolder, entryName, ContentModel.TYPE_CONTENT);
                inputStream = transformService.cleanDitaMap(zis.readAllBytes(), entryName);
                logger.debug("Created file {} [{}]", entryName, file);
                zipOut.putNextEntry(new ZipEntry(entryName));
                isMap = true;
            } else if (entryName.contains(".dita")) {
                file = createContent(topics, entryName, ContentModel.TYPE_CONTENT);
                inputStream = new ByteArrayInputStream(zis.readAllBytes());
                zipOut.putNextEntry(new ZipEntry("topics/" + entryName));
                isTopic = true;
            } else {
                file = createContent(images, entryName, ContentModel.TYPE_CONTENT);
                inputStream = new ByteArrayInputStream(zis.readAllBytes());
                zipOut.putNextEntry(new ZipEntry("images/" + entryName));
            }
            ContentWriter fileWriter = contentService.getWriter(file, ContentModel.PROP_CONTENT, true);
            fileWriter.guessMimetype(entryName);
            fileWriter.putContent(inputStream);
            ContentReader fileReader = contentService.getReader(file, ContentModel.PROP_CONTENT);
            zipOut.write(fileReader.getContentInputStream().readAllBytes());

            // generating html rendition
            if (entryName.contains(".dita")) {
                InputStream htmlRendition = transformService.generateHtmlRendition(file, entryName);
//                try {
//                    htmlRendition = tidyService.clean(htmlRendition);
//                } catch (InterruptedException e) {
//                    logger.error("Tidy cleanup failed", e);
//                }
                String htmlName = FilenameUtils.getBaseName(entryName) + ".html";
                NodeRef parentFolderOfExtractedFile = nodeService.getPrimaryParent(file).getParentRef();
                NodeRef htmlRenditionNodeRef = createContent(parentFolderOfExtractedFile, htmlName, ContentModel.TYPE_CONTENT);
                logger.debug("Created file {} [{}]", htmlName, htmlRenditionNodeRef);
                ContentWriter htmlRenditionWriter = contentService.getWriter(htmlRenditionNodeRef, ContentModel.PROP_CONTENT, true);
                htmlRenditionWriter.guessMimetype(htmlName);
                htmlRenditionWriter.putContent(htmlRendition);

                if (isMap) {
                    zipOut.putNextEntry(new ZipEntry(htmlName));
                } else if (isTopic) {
                    zipOut.putNextEntry(new ZipEntry("topics/" + htmlName));
                } else {
                    zipOut.putNextEntry(new ZipEntry("images/" + htmlName));
                }
                ContentReader htmlReader = contentService.getReader(htmlRenditionNodeRef, ContentModel.PROP_CONTENT);
                zipOut.write(htmlReader.getContentInputStream().readAllBytes());
            }
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
        zipOut.close();

        return resultZip;
    }

    private NodeRef createContent(NodeRef parentFolder, String resultName, QName type) {
        Map<QName, Serializable> defaultProps = new HashMap<>();
        defaultProps.put(ContentModel.PROP_NAME, resultName);
        return serviceRegistry.getNodeService().createNode(parentFolder,
                ContentModel.ASSOC_CONTAINS,
                QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, resultName),
                type, defaultProps).getChildRef();
    }

    private NodeRef collectMapAndDependencies(String nodeRef) {
        AlfrescoPipelineSource zipPipelineSource = pipelineService.getZipPipelineSource(new NodeRef(nodeRef));
        long start = System.currentTimeMillis();
        try {
            pipelineService.runPipeline(zipPipelineSource);
        } finally {
            logger.debug(String.format("\tPipeline for '%s' has been executed. It took %s ms.", zipPipelineSource,
                    System.currentTimeMillis() - start));
        }
        return serviceRegistry.getFileFolderService().list(zipPipelineSource.getResultBase())
                .stream().filter(fileInfo -> fileInfo.getName().equals(zipPipelineSource.getResultDocumentName())).findFirst().get().getNodeRef();
    }

    public void setPipelineService(PipelineService pipelineService) {
        this.pipelineService = pipelineService;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setComponize2spider(Map<String, Map<String, String>> componize2spider) {
        this.componize2spider = componize2spider;
    }

    public Map getComponize2spider() {
        return componize2spider;
    }

    public void setPublicationClient(PublicationClient publicationClient) {
        this.publicationClient = publicationClient;
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public void setTransformService(TransformService transformService) {
        this.transformService = transformService;
    }

    public void setCleanTempSources(Boolean cleanTempSources) {
        this.cleanTempSources = cleanTempSources;
    }

    public void setS3Client(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public void setXdbUploadService(XDBUploadService xdbUploadService) {
        this.xdbUploadService = xdbUploadService;
    }

    public void setTidyService(TidyService tidyService) {
        this.tidyService = tidyService;
    }

    public void setOxygenService(OxygenService oxygenService) {
        this.oxygenService = oxygenService;
    }

    public void setDitaOtService(DitaOtService ditaOtService) {
        this.ditaOtService = ditaOtService;
    }
}
