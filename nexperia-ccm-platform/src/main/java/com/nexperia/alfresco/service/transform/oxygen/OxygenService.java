package com.nexperia.alfresco.service.transform.oxygen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class OxygenService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private boolean isServiceEnabled = false;
    private String antLauncherJarLocation;

    public String transformDitaToHtml5(String sourceMapPath, String outputFolder) throws IOException, InterruptedException {
        if (isServiceEnabled) {
            Runtime rt = Runtime.getRuntime();
            List<String> cmd = new ArrayList<String>();

            cmd.add("java");
            cmd.add("-jar");
            cmd.add(antLauncherJarLocation);
            cmd.add(sourceMapPath);
            System.out.println("Source file path: " + sourceMapPath);
            cmd.add(outputFolder);
            System.out.println("Output folder: " + outputFolder);
            String[] cmdArgs = cmd.toArray(new String[0]);
            System.out.println("Executing: " + cmd.toString().replaceAll(",", " "));
            Process proc = rt.exec(cmdArgs);
            if (logger.isDebugEnabled()) {
                logger.debug("Command executed : " + cmd.toString().replaceAll(",", " "));
            }
            StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");
            StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

            // kick them off
            errorGobbler.start();
            outputGobbler.start();

            // any error???
            int exitVal = proc.waitFor();
            if (logger.isDebugEnabled()) {
                logger.debug("Process exitValue: " + exitVal);
            }

            // success
            if (logger.isDebugEnabled()) {
                logger.debug("Oxygen conversion executed successfully.");
            }

            return outputFolder;
        }

        return null;
    }

    private class StreamGobbler extends Thread {
        InputStream is;
        String type;
        OutputStream os;

        StreamGobbler(InputStream is, String type) {
            this(is, type, System.out);
        }

        StreamGobbler(InputStream is, String type, OutputStream redirect) {
            this.is = is;
            this.type = type;
            this.os = redirect;
        }

        public void run() {
            try {
                PrintWriter pw = null;
                if (os != null)
                    pw = new PrintWriter(os);

                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    if (pw != null)
                        pw.println(line);
                }
                if (pw != null)
                    pw.flush();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void setServiceEnabled(boolean serviceEnabled) {
        isServiceEnabled = serviceEnabled;
    }

    public void setAntLauncherJarLocation(String antLauncherJarLocation) {
        this.antLauncherJarLocation = antLauncherJarLocation;
    }

}
