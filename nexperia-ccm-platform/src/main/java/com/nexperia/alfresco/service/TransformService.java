package com.nexperia.alfresco.service;

import com.nexperia.alfresco.util.Util;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.TempFileProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class TransformService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ServiceRegistry serviceRegistry;
    private Util util;
    private String xsltFilesSpace;
    private String MAP_CLEAN_XSLT;
    private String MAP_CONVERT_XSLT;
    //    private String TOPIC_CLEAN_XSLT;
    private String TOPIC_CONVERT_XSLT;

    public InputStream cleanDitaMap(byte[] bytes, String name) {
        NodeRef xslFile = util.getNodeByPath(MAP_CLEAN_XSLT);
//        System.setProperty("javax.xml.transform.TransformerFactory",
//                "net.sf.saxon.TransformerFactoryImpl");
        TransformerFactory factory = TransformerFactory.newInstance();
        ContentReader xsltCR = serviceRegistry.getContentService().getReader(xslFile, ContentModel.PROP_CONTENT);
        Source xslt = new StreamSource(xsltCR.getContentInputStream());
        Transformer transformer = null;
        File tempFile = TempFileProvider.createTempFile("cleaned_" + name, ".out");
        try {
            transformer = factory.newTransformer(xslt);
            Source source = new StreamSource(new ByteArrayInputStream(bytes));
            Result resultOS = new StreamResult(new FileOutputStream(tempFile));
            transformer.transform(source, resultOS);
            return new FileInputStream(tempFile);
        } catch (Exception e) {
            logger.error(name + " Dita Map cleaning XSLT transformation filed", e);
            return null;
        }
    }

    public InputStream generateHtmlRendition(NodeRef sourceDita, String ditaName) {
        NodeRef xslFile = ditaName.contains(".ditamap") ? util.getNodeByPath(MAP_CONVERT_XSLT) : util.getNodeByPath(TOPIC_CONVERT_XSLT);
        TransformerFactory factory = TransformerFactory.newInstance();
        ContentReader xsltCR = serviceRegistry.getContentService().getReader(xslFile, ContentModel.PROP_CONTENT);
        Source xslt = new StreamSource(xsltCR.getContentInputStream());
        Transformer transformer = null;
        File tempFile = TempFileProvider.createTempFile("html_" + ditaName, ".out");
        try {
            transformer = factory.newTransformer(xslt);
            ContentReader ditaSourceReader = serviceRegistry.getContentService().getReader(sourceDita, ContentModel.PROP_CONTENT);
            Source source = new StreamSource(ditaSourceReader.getContentInputStream());
            Result resultOS = new StreamResult(new FileOutputStream(tempFile));
            transformer.transform(source, resultOS);
            return new FileInputStream(tempFile);
        } catch (Exception e) {
            logger.error("{} Dita Map cleaning XSLT transformation filed", ditaName, e);
            return null;
        }
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setXsltFilesSpace(String xsltFilesSpace) {
        this.xsltFilesSpace = xsltFilesSpace;
        this.MAP_CLEAN_XSLT = xsltFilesSpace + "/cm:mapClean.xslt";
        this.MAP_CONVERT_XSLT = xsltFilesSpace + "/cm:mapToHtml.xslt";
//        this.TOPIC_CLEAN_XSLT = xsltFilesSpace + "/cm:topicClean.xslt";
        this.TOPIC_CONVERT_XSLT = xsltFilesSpace + "/cm:topicToHtml.xslt";
    }

    public String getXsltFilesSpace() {
        return xsltFilesSpace;
    }

    public void setUtil(Util util) {
        this.util = util;
    }
}
