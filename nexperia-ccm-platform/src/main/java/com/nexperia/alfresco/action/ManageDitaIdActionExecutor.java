package com.nexperia.alfresco.action;

import com.nexperia.alfresco.service.DitaService;
import com.nexperia.alfresco.util.Util;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static com.nexperia.alfresco.util.CCMConstants.*;

public class ManageDitaIdActionExecutor extends ActionExecuterAbstractBase {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private DitaService ditaService;
    private Util util;
    private NodeService nodeService;

    @Override
    protected void executeImpl(Action action, NodeRef dita) {
        String fileName = util.getName(dita);
        if (ditaService.isDitaMapFile(fileName) && !nodeService.hasAspect(dita, ContentModel.ASPECT_WORKING_COPY)) {
            Map<String, String> dTypes = ditaService.parseDocumentTypeOfTheDitaMap(dita);
            if (DOCUMENT_TYPE_DATA_SHEET.equals(dTypes.get(PUBLICATION_TYPE_TAG)) && !nodeService.hasAspect(dita, ASPECT_DATA_SHEET)) {
                logger.debug("Checkin {} Data sheet ditaMap id", fileName);
                if (!ditaService.isMapIdValid(dita, "ds_")) {
                    ditaService.updateDataSheetIdParameter(dita, "ds_");
                }
            }
        } else if (ditaService.isDitaTopicFile(fileName) && !nodeService.hasAspect(dita, ContentModel.ASPECT_WORKING_COPY)) {
            logger.debug("Checkin {} topic", fileName);
            if (!ditaService.isTopicIdValid(dita)) {
                ditaService.updateTopicId(dita);
            }
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
    }

    public void setDitaService(DitaService ditaService) {
        this.ditaService = ditaService;
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }
}
