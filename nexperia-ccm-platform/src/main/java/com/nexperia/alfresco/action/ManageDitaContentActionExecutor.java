package com.nexperia.alfresco.action;

import com.componize.alfresco.repo.rdf.RdfModel;
import com.componize.alfresco.repo.xml.XmlModel;
import com.nexperia.alfresco.service.ManageDitaContentService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ManageDitaContentActionExecutor extends ActionExecuterAbstractBase {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ManageDitaContentService manageDitaContentService;
    private NodeService nodeService;

    @Override
    protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
        manageDitaContentService.checkContentAndMarkDitaMap(actionedUponNodeRef);
        if (!nodeService.hasAspect(actionedUponNodeRef, ContentModel.ASPECT_VERSIONABLE)) {
            nodeService.addAspect(actionedUponNodeRef, ContentModel.ASPECT_VERSIONABLE, null);
        }
        if (!nodeService.hasAspect(actionedUponNodeRef, RdfModel.ASPECT_CONTENT)) {
            nodeService.addAspect(actionedUponNodeRef, RdfModel.ASPECT_CONTENT, null);
        }
        if (!nodeService.hasAspect(actionedUponNodeRef, XmlModel.ASPECT_CONTENT)) {
            nodeService.addAspect(actionedUponNodeRef, XmlModel.ASPECT_CONTENT, null);
        }
        if (!nodeService.hasAspect(actionedUponNodeRef, XmlModel.ASPECT_VALIDATION)) {
            nodeService.addAspect(actionedUponNodeRef, XmlModel.ASPECT_VALIDATION, null);
        }
        logger.debug("Added all necessary aspects to {}", actionedUponNodeRef);
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setManageDitaContentService(ManageDitaContentService manageDitaContentService) {
        this.manageDitaContentService = manageDitaContentService;
    }
}
