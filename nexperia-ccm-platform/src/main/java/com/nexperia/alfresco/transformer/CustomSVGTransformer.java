package com.nexperia.alfresco.transformer;

import org.alfresco.repo.content.transform.AbstractContentTransformer2;
import org.alfresco.service.cmr.repository.ContentIOException;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.TransformationOptions;
import org.alfresco.util.TempFileProvider;
import org.alfresco.util.exec.RuntimeExec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.alfresco.repo.content.MimetypeMap.MIMETYPE_IMAGE_JPEG;
import static org.alfresco.repo.content.MimetypeMap.MIMETYPE_IMAGE_SVG;

public class CustomSVGTransformer extends AbstractContentTransformer2 {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private RuntimeExec executer;

    @Override
    public boolean isTransformable(String sourceMimetype, String targetMimetype, TransformationOptions options) {
        boolean isTransormable = MIMETYPE_IMAGE_SVG.equals(sourceMimetype) && MIMETYPE_IMAGE_JPEG.equals(targetMimetype);
        if (isTransormable) {
            logger.debug("isTransformable: Source mimetype '{}', target mimetype is '{}'", sourceMimetype, targetMimetype);
        }
        return isTransormable;
    }

    @Override
    protected void transformInternal(ContentReader contentReader, ContentWriter contentWriter, TransformationOptions transformationOptions) throws Exception {
        File contentToTransform = TempFileProvider.createTempFile(contentReader.getContentInputStream(), "_source_", ".jpg");
        File convertedFile = getConvertedFile(contentToTransform);
        contentWriter.putContent(convertedFile);
        logger.debug("SVG to JPG transform done!");
    }

    public File getConvertedFile(File source) throws Exception {
        String sourceFile = source.getAbsolutePath();
        String targetFile = source.getAbsolutePath().substring(0, source.getAbsolutePath().length() - source.getName().length()) + (new Date()).getTime() + ".jpg";

        Map<String, String> properties = new HashMap(2);
        properties.put("source", sourceFile);
        properties.put("target", targetFile);
        RuntimeExec.ExecutionResult result = this.executer.execute(properties);
        if (result.getExitValue() != 0 && result.getStdErr() != null && result.getStdErr().length() > 0) {
            throw new ContentIOException("Failed to perform ImageMagick transformation: \n" + result);
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("ImageMagick executed successfully: \n" + this.executer);
            }
        }

        File convertedFile = new File(targetFile);
        if (!convertedFile.exists()) {
            throw new FileNotFoundException(targetFile + " was not found");
        }

        // success
        if (logger.isDebugEnabled()) {
            logger.debug("convert command executed successfully.");
        }

        return convertedFile;
    }

    public void setExecuter(RuntimeExec executer) {
        executer.setProcessProperty("MAGICK_TMPDIR", TempFileProvider.getTempDir().getAbsolutePath());
        this.executer = executer;
    }
}
