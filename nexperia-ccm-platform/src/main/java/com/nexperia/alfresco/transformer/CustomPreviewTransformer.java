package com.nexperia.alfresco.transformer;

import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.content.transform.AbstractContentTransformer2;
import org.alfresco.repo.security.permissions.AccessDeniedException;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.TransformationOptions;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

public class CustomPreviewTransformer extends AbstractContentTransformer2 {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private ServiceRegistry serviceRegistry;

    private String ssgPdfServiceUrl = "https://%s.eptos.pim.ssg.nexperia.com/ssg/map/ccmshare.pdf?type=ccmshare&custom=%s&nodeRef=%s&purgecache=yes&user=ssg&password=";
    private String qaCreds = "%40Nex17%23peria";
    private String prodCreds = "LRS%26Rq27PRnr4p%21L";
    private String env;

    @Override
    public boolean isTransformable(String sourceMimetype, String targetMimetype, TransformationOptions options) {
        return  "application/dita+xml".equals(sourceMimetype) && MimetypeMap.MIMETYPE_PDF.equals(targetMimetype);
    }

    @Override
    protected void transformInternal(ContentReader contentReader, ContentWriter contentWriter, TransformationOptions transformationOptions) throws Exception {
        logger.debug("Starting CustomPreviewTransformer transformation...");
        NodeRef sourceNR = transformationOptions.getSourceNodeRef();
        InputStream newRendition = downloadPdf(sourceNR);
        if (newRendition == null) {
            logger.debug("End CustomPreviewTransformer transformation... PDF generation failed.");
        } else {
            contentWriter.putContent(newRendition);
            logger.debug("End CustomPreviewTransformer transformation... PDF was written to output");
        }
    }

    private InputStream downloadPdf(NodeRef sourceNR) {
        String webdavUrl = null;
        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
        try {
            List<FileInfo> paths = fileFolderService.getNamePath(null, sourceNR);

            // build up the webdav url
            StringBuilder path = new StringBuilder();

            // build up the path skipping the first path as it is the root
            // folder
            for (int x = 1; x < paths.size(); x++) {
                if (x != 1) {
                    path.append("/");
                }
                path.append(URLEncoder.encode(paths.get(x).getName()));
            }
            webdavUrl = path.toString();
        } catch (AccessDeniedException e) {
            // cannot build path if user don't have access all the way up
        } catch (FileNotFoundException nodeErr) {
            // cannot build path if file no longer exists
        }

        String downloadURL = String.format(ssgPdfServiceUrl, ("prod".equals(env) ? "www" : "qa"), webdavUrl, sourceNR) + ("prod".equals(env) ? prodCreds : qaCreds);
        logger.debug("Getting pdf from SSG by following URL: " + downloadURL);

        try {
            TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    return true;
                }
            };
            SSLSocketFactory sf = null;
            sf = new SSLSocketFactory(acceptingTrustStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
            registry.register(new Scheme("https", 443, sf));
            ClientConnectionManager ccm = new PoolingClientConnectionManager(registry);
            DefaultHttpClient httpClient = new DefaultHttpClient(ccm);
            HttpGet getMethod = new HttpGet(downloadURL);

            HttpResponse response = httpClient.execute(getMethod);
            if (response.getStatusLine().getStatusCode() == 200) {
                InputStream content = response.getEntity().getContent();
                if (content != null) {
                    return content;
                }
            } else {
                logger.error("There is a problem to download PDF document by URL '" + downloadURL + "'. Error message: status code: " + response.getStatusLine().getStatusCode());
            }

        } catch (Exception e) {
            logger.error("There is a problem to download PDF document by URL '" + downloadURL + "'", e);
        }

        return null;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setEnv(String env) {
        this.env = env;
    }

}
