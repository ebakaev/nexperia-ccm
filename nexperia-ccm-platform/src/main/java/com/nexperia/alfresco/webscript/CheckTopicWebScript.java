package com.nexperia.alfresco.webscript;

import com.nexperia.alfresco.util.Util;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang3.StringUtils;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckTopicWebScript extends DeclarativeWebScript {

    Util util;
    ServiceRegistry serviceRegistry;

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        String nodeRefStr = req.getParameter("nodeRef");
        result.put("message", Util.VALID);
        if (StringUtils.isNotBlank(nodeRefStr)) {
            NodeRef nodeRef = findOITopic(new NodeRef(nodeRefStr));
            if (nodeRef != null) {
                ContentReader contentReader = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
                String checkResult = util.checkTopicTransform(util.getName(nodeRefStr), contentReader);
                result.put("message", checkResult);
            }
        }

        return result;
    }

    private NodeRef findOITopic(NodeRef vpNodeRef) {
        NodeRef parent = serviceRegistry.getNodeService().getPrimaryParent(vpNodeRef).getParentRef();
        List<FileInfo> vpFolders = serviceRegistry.getFileFolderService().listFolders(parent);
        FileInfo topicsFolder = vpFolders.stream().filter(fileInfo -> StringUtils.equals(fileInfo.getName(), "topics")).findFirst().orElse(null);
        if (topicsFolder != null) {
            List<FileInfo> topics = serviceRegistry.getFileFolderService().listFiles(topicsFolder.getNodeRef());
            FileInfo oiTopic = topics.stream().filter(fileInfo -> StringUtils.startsWith(fileInfo.getName(), "oi_")).findFirst().orElse(null);
            if (oiTopic != null) {
                return oiTopic.getNodeRef();
            }
        }

        return null;
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
