package com.nexperia.alfresco.webscript;

import com.nexperia.alfresco.util.Util;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class UpdateTopicsWebScript extends AbstractWebScript {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    Util util;
    ServiceRegistry serviceRegistry;
    private String QUERY_TEMPLATE = "PATH:\"/%s//*\" and @cm\\:name:\"oi_*.dita\""; // /app:company_home/st:sites/cm:en-US/cm:documentLibrary/cm:Source/cm:_x0037_4ALVC08//*

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String path = webScriptRequest.getParameter("path");

        if (StringUtils.isEmpty(path)) {
            throw new WebScriptException(HttpStatus.BAD_REQUEST.value(), "Path is mandatory parameter");
        }

        String queryString = String.format(QUERY_TEMPLATE, path);
        List<NodeRef> topicsToUpdate = util.searchByQuery(queryString);
        topicsToUpdate.forEach(topic -> {
            String topicName = util.getName(topic);
            logger.debug("updating => {} [{}] path: {}", topicName, topic, serviceRegistry.getNodeService().getPath(topic).toPrefixString(serviceRegistry.getNamespaceService()));
            transformAndUpdate(topicName, topic);
        });
    }

    private void transformAndUpdate(String topicName, NodeRef topic) {
        ContentReader contentReader = serviceRegistry.getContentService().getReader(topic, ContentModel.PROP_CONTENT);
        InputStream updatedTopicIS = util.updateTopicTransform(topicName, contentReader);
        String prettyPrintOutput = util.format(updatedTopicIS);
        ContentWriter contentWriter = serviceRegistry.getContentService().getWriter(topic, ContentModel.PROP_CONTENT, true);
        contentWriter.putContent(prettyPrintOutput);
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

}
