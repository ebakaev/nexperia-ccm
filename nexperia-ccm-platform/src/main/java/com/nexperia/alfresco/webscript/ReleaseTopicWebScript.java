package com.nexperia.alfresco.webscript;

import com.nexperia.alfresco.service.ReleaseService;
import com.nexperia.alfresco.util.Util;
import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.util.StringUtils;

import java.io.IOException;


/**
 * Publishes a topic to SPIDER Library
 */
public class ReleaseTopicWebScript extends AbstractWebScript {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ReleaseService releaseService;
    private Util util;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String nodeRef = webScriptRequest.getParameter("nodeRef");
        if (StringUtils.isEmpty(nodeRef)) {
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, "NodeRef isn't specified! It's mandatory parameter.");
        }

        logger.debug("Starting release of: {} [{}]", util.getName(nodeRef), nodeRef);
        releaseService.releaseTopic(nodeRef);
        logger.debug("Release of: {} [{}] completed successfully!", util.getName(nodeRef), nodeRef);
    }

    public void setReleaseService(ReleaseService releaseService) {
        this.releaseService = releaseService;
    }

    public void setUtil(Util util) {
        this.util = util;
    }
}
