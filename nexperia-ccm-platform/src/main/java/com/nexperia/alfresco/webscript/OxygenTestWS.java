package com.nexperia.alfresco.webscript;

import com.nexperia.alfresco.service.transform.oxygen.OxygenService;
import com.nexperia.alfresco.util.Util;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

public class OxygenTestWS extends AbstractWebScript {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ServiceRegistry serviceRegistry;
    private OxygenService oxygenService;
    private Util util;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        logger.debug("Starting oxygen");
        NodeRef nodeRef = new NodeRef("workspace://SpacesStore/05b9be8d-0686-4cc4-a34c-7380daf46ffe");
        ContentReader contentReader = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
//        try {
//            oxygenService.transformDitaToHtml5(util.getName(nodeRef), contentReader.getContentInputStream());
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        logger.debug("completed successfully!");
    }

    public void setOxygenService(OxygenService oxygenService) {
        this.oxygenService = oxygenService;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setUtil(Util util) {
        this.util = util;
    }
}
