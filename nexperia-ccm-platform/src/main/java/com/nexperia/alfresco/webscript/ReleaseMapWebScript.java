package com.nexperia.alfresco.webscript;

import com.nexperia.alfresco.service.DitaService;
import com.nexperia.alfresco.service.ReleaseService;
import com.nexperia.alfresco.util.Util;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 1. Runs dita2zip componize pipeline to collect all related topics and images
 * 2. Creates html renditions for each dita files
 * 3. Packs all into suitable zip format
 * 4. Publish to SPIDER Library
 */
public class ReleaseMapWebScript extends DeclarativeWebScript {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ReleaseService releaseService;
    private Util util;
    private DitaService ditaService;

    protected Map<String, Object> executeImpl(final WebScriptRequest req, final Status status, final Cache cache) {
        String nodeRef = req.getParameter("nodeRef");
        if (StringUtils.isEmpty(nodeRef)) {
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, "NodeRef isn't specified! It's mandatory parameter.");
        }
        boolean isDatasheet = false;
        if (ditaService.isDataSheetDocument(nodeRef)) {
            isDatasheet = true;
            logger.debug("Starting release Data sheet: {}[{}]", util.getName(nodeRef), nodeRef);
        } else {
            logger.debug("Starting release Value proposition: {}[{}]", util.getName(nodeRef), nodeRef);
//            nodeRef = ditaService.getLinkedVPNodeRef(nodeRef);
        }
        String publicationId = null;
        Map<String, Object> result = new HashMap<>();
        boolean isSuccess = true;
        try {
            publicationId = releaseService.releaseMapUsingDitaOt(nodeRef, isDatasheet);
        } catch (IOException e) {
            isSuccess = false;
            e.printStackTrace();
        }
        logger.debug("Release of: {}[{}] completed successfully!", util.getName(nodeRef), nodeRef);

        if (isSuccess) {
            result.put("res", "Success");
            result.put("msg", "Success");
            result.put("pubId", StringUtils.isNoneEmpty(publicationId) ? publicationId : "");
        } else {
            result.put("res", "Failure");
            result.put("msg", "Publication failed for {" + nodeRef + "}, check logs please...");
            result.put("pubId", "");
        }

        return result;
    }

    public void setReleaseService(ReleaseService releaseService) {
        this.releaseService = releaseService;
    }

    public void setUtil(Util util) {
        this.util = util;
    }

    public void setDitaService(DitaService ditaService) {
        this.ditaService = ditaService;
    }
}
