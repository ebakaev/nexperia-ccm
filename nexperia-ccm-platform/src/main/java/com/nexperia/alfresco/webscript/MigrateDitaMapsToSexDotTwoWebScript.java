package com.nexperia.alfresco.webscript;

import com.nexperia.alfresco.util.CCMConstants;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;

public class MigrateDitaMapsToSexDotTwoWebScript extends AbstractWebScript {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ServiceRegistry serviceRegistry;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String nodeRef = webScriptRequest.getParameter("nodeRef");
        if (StringUtils.isEmpty(nodeRef)) {
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, "NodeRef isn't specified! It's mandatory parameter.");
        }
        processFolder(new NodeRef(nodeRef));
    }

    private void processFolder(NodeRef folder) {
        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
        List<FileInfo> list = fileFolderService.list(folder);
        for (FileInfo file : list) {
            if (file.isFolder()) {
                processFolder(file.getNodeRef());
            } else {
                if (StringUtils.endsWithIgnoreCase(file.getName(), ".ditamap")) {
                    migrateMap(file.getNodeRef(), file.getName());
                }
            }
        }
    }

    private void migrateMap(NodeRef nodeRef, String ditamapName) {
        NodeService nodeService = serviceRegistry.getNodeService();
        if (!nodeService.hasAspect(nodeRef, ContentModel.ASPECT_WORKING_COPY) && !serviceRegistry.getLockService().isLocked(nodeRef)) {
            logger.debug("Migrating {} [{}]", ditamapName, nodeRef);
            if (nodeService.hasAspect(nodeRef, CCMConstants.ASPECT_LEGACY_VALUE_PROPOSITION)) {
                nodeService.addAspect(nodeRef, CCMConstants.ASPECT_VALUE_PROPOSITION, null);
                nodeService.removeAspect(nodeRef, CCMConstants.ASPECT_LEGACY_VALUE_PROPOSITION);
            }
            if (nodeService.hasAspect(nodeRef, CCMConstants.ASPECT_LEGACY_DATA_SHEET)) {
                nodeService.addAspect(nodeRef, CCMConstants.ASPECT_DATA_SHEET, null);
                nodeService.removeAspect(nodeRef, CCMConstants.ASPECT_LEGACY_DATA_SHEET);
            }
            if (nodeService.hasAspect(nodeRef, CCMConstants.ASPECT_LEGACY_APPLICATION_NOTE)) {
                nodeService.addAspect(nodeRef, CCMConstants.ASPECT_APPLICATION_NOTE, null);
                nodeService.removeAspect(nodeRef, CCMConstants.ASPECT_LEGACY_APPLICATION_NOTE);
            }
            if (nodeService.hasAspect(nodeRef, CCMConstants.ASPECT_LEGACY_SUBJECT_SCHEME)) {
                nodeService.addAspect(nodeRef, CCMConstants.ASPECT_SUBJECT_SCHEME, null);
                nodeService.removeAspect(nodeRef, CCMConstants.ASPECT_LEGACY_SUBJECT_SCHEME);
            }
            if (nodeService.hasAspect(nodeRef, CCMConstants.ASPECT_LEGACY_PACKING_INFORMATION)) {
                nodeService.addAspect(nodeRef, CCMConstants.ASPECT_PACKING_INFORMATION, null);
                nodeService.removeAspect(nodeRef, CCMConstants.ASPECT_LEGACY_PACKING_INFORMATION);
            }
        }
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
