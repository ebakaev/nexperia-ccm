<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="text" version="1.0" encoding="UTF-8"/>

    <xsl:template match="/">
        <xsl:for-each select="//xref">
            <xsl:variable name="hrefValue">
                <xsl:call-template name="substring-after-last">
                    <xsl:with-param name="string" select="@href" />
                    <xsl:with-param name="delimiter" select="'/'" />
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="xrefValue" select="normalize-space(.)"/>
            <xsl:if test="$xrefValue != $hrefValue">invalid</xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="substring-after-last">
        <xsl:param name="string" />
        <xsl:param name="delimiter" />
        <xsl:choose>
            <xsl:when test="contains($string, $delimiter)">
                <xsl:call-template name="substring-after-last">
                    <xsl:with-param name="string"
                                    select="substring-after($string, $delimiter)" />
                    <xsl:with-param name="delimiter" select="$delimiter" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise><xsl:value-of
                    select="$string" /></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
