<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="no"
                doctype-public="-//OASIS//DTD DITA 1.3 Topic//EN"
                doctype-system="http://www.data.nexperia.com/schemas/dita/v1.3/dtd/technicalContent/dtd/topic.dtd"/>

    <xsl:template match="@class" />
    <xsl:template match="@domains" />
    <xsl:template match="@rowheader" />
    <xsl:variable name="countOfEntry" select="count(topic/body/table/tgroup/tbody/row/entry)"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="topic/body/table/tgroup/tbody/row/entry[position() = 1]">
        <xsl:variable name="moreRowsValue" select="normalize-space(@morerows)"/>
        <xsl:variable name="colNameValue" select="normalize-space(@colname)"/>
        <entry>
            <xsl:if test="$colNameValue != ''">
                <xsl:attribute name="colname"><xsl:value-of select="$colNameValue"/></xsl:attribute>
            </xsl:if>
            <xsl:if test="$moreRowsValue != ''">
                <xsl:attribute name="morerows"><xsl:value-of select="$moreRowsValue"/></xsl:attribute>
            </xsl:if>
            <xsl:call-template name="updateFirstPTag"/>
        </entry>
    </xsl:template>

    <xsl:template match="topic/body/table/tgroup/tbody/row/entry[position() > 2 and position() = last()]">
        <xsl:variable name="moreRowsValue" select="normalize-space(@morerows)"/>
        <xsl:variable name="colNameValue" select="normalize-space(@colname)"/>
        <entry>
            <xsl:if test="$colNameValue != ''">
                <xsl:attribute name="colname"><xsl:value-of select="$colNameValue"/></xsl:attribute>
            </xsl:if>
            <xsl:if test="$moreRowsValue != ''">
                <xsl:attribute name="morerows"><xsl:value-of select="$moreRowsValue"/></xsl:attribute>
            </xsl:if>
            <xsl:call-template name="updateLastPTag"/>
        </entry>
    </xsl:template>

    <xsl:template match="topic/body/bodydiv/table/tgroup/tbody/row/entry[position() = 1]">
        <xsl:variable name="moreRowsValue" select="normalize-space(@morerows)"/>
        <xsl:variable name="colNameValue" select="normalize-space(@colname)"/>
        <entry>
            <xsl:if test="$colNameValue != ''">
                <xsl:attribute name="colname"><xsl:value-of select="$colNameValue"/></xsl:attribute>
            </xsl:if>
            <xsl:if test="$moreRowsValue != ''">
                <xsl:attribute name="morerows"><xsl:value-of select="$moreRowsValue"/></xsl:attribute>
            </xsl:if>
            <xsl:call-template name="updateFirstPTag"/>
        </entry>
    </xsl:template>

    <xsl:template match="topic/body/bodydiv/table/tgroup/tbody/row/entry[position() > 2 and position() = last()]">
        <xsl:variable name="moreRowsValue" select="normalize-space(@morerows)"/>
        <xsl:variable name="colNameValue" select="normalize-space(@colname)"/>
        <entry>
            <xsl:if test="$colNameValue != ''">
                <xsl:attribute name="colname"><xsl:value-of select="$colNameValue"/></xsl:attribute>
            </xsl:if>
            <xsl:if test="$moreRowsValue != ''">
                <xsl:attribute name="morerows"><xsl:value-of select="$moreRowsValue"/></xsl:attribute>
            </xsl:if>
            <xsl:call-template name="updateLastPTag"/>
        </entry>
    </xsl:template>

    <xsl:template name="updateFirstPTag">
        <xsl:for-each select="p">
            <xsl:variable name="pValue" select="normalize-space(.)"/>
            <xsl:variable name="productUrl" select="concat('https://www.nexperia.com/product/', $pValue)"/>
            <p>
                <xsl:choose>
                    <xsl:when test="not(contains($pValue, ' '))">
                            <xsl:choose>
                                <xsl:when test="xref">
                                   <xsl:apply-templates/>
                                </xsl:when>
                                <xsl:when test="$pValue = ''">
                                    <xsl:attribute name="conkeyref"><xsl:value-of select="@conkeyref"/></xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:if test="@product != ''">
                                        <xsl:attribute name="product"><xsl:value-of select="@product"/></xsl:attribute>
                                    </xsl:if>
                                    <xref>
                                        <xsl:attribute name="href"><xsl:value-of select="$productUrl"/></xsl:attribute>
                                        <xsl:attribute name="format">html</xsl:attribute>
                                        <xsl:attribute name="scope">external</xsl:attribute>
                                        <xsl:value-of select="$pValue"/>
                                    </xref>
                                </xsl:otherwise>
                            </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates/>
                    </xsl:otherwise>
                </xsl:choose>
            </p>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="updateLastPTag">
        <xsl:variable name="pValue" select="normalize-space(.)"/>
        <xsl:variable name="productUrl" select="concat('https://www.nexperia.com/package/', $pValue)"/>
        <xsl:choose>
            <xsl:when test="$pValue != ''">
                <p>
                    <xsl:choose>
                        <xsl:when test="$countOfEntry > 2">
                            <xsl:choose>
                                <xsl:when test="xref">
                                    <xsl:apply-templates/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:if test="@product != ''">
                                        <xsl:attribute name="product"><xsl:value-of select="@product"/></xsl:attribute>
                                    </xsl:if>
                                    <xref>
                                        <xsl:attribute name="href"><xsl:value-of select="$productUrl"/></xsl:attribute>
                                        <xsl:attribute name="format">html</xsl:attribute>
                                        <xsl:attribute name="scope">external</xsl:attribute>
                                        <xsl:value-of select="$pValue"/>
                                    </xref>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$pValue"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </p>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
